package com.asac.minfi.services.interfaces;

import java.util.List;

import com.asac.minfi.entities.Vehicules;
import com.asac.minfi.entities.VehiculesResponse;

public interface VehiculesService {

	public VehiculesResponse createVehicules(Vehicules vehicule);
	
	public VehiculesResponse updateVehicules(Vehicules vehicule);
	
	public List<Vehicules> listVehicules();
	
	public List<Vehicules> listVehicules(String immatriculation);
	
	public VehiculesResponse listVehiculeFromMINFI(String immatriculation);

}
