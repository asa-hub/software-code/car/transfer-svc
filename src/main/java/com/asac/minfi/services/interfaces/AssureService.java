package com.asac.minfi.services.interfaces;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;

public interface AssureService {

	public AssureResponse createAssure(Assure assure);
	
	///public CompletableFuture<AssureResponse> createAssure(Assure assure);

	
	public AssureResponse updateAssure(Assure assure);
	
	public List<Assure> listAssure();
	
	public List<Assure> listAssure(String code_assure, String code_assureur);
	
	public AssureResponse listAssureFromMINFI(String code_assure, String code_assureur);

	
}
