package com.asac.minfi.services.interfaces;

import java.util.List;

import com.asac.minfi.entities.Attestation;
import com.asac.minfi.entities.AttestationResponse;

public interface AttestationsService {

	public AttestationResponse createAttestation(Attestation attestation);
	
	public AttestationResponse updateAttestation(Attestation attestation);
	
	public List<Attestation> listAttestation();
	
	public List<Attestation> listAttestation(String numero_attestation);
}
