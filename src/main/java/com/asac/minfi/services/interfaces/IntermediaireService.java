package com.asac.minfi.services.interfaces;

import java.util.List;

import com.asac.minfi.entities.Intermediaire;
import com.asac.minfi.entities.IntermediaireResponse;

public interface IntermediaireService {

	public IntermediaireResponse createIntermediaire(Intermediaire intermediaire);
	
	public IntermediaireResponse updateIntermediaire(Intermediaire intermediaire);
	
	public List<Intermediaire> listIntermediaire();
	
	public List<Intermediaire> listIntermediaire(String identifiant_dna);
}
