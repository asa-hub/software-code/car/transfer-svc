package com.asac.minfi.services.interfaces;

import java.util.List;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.entities.SinistresResponse;

public interface SinistresService {

	public SinistresResponse createSinistres(Sinistres sinistres);
	
	public SinistresResponse updateSinistres(Sinistres sinistres);
	
	public List<Sinistres> listSinistres();
	
	public List<Sinistres> listSinistres(String numero_attestation, String reference);

	public SinistresResponse listSinistreFromMINFI(String numero_attestation, String reference);
}
