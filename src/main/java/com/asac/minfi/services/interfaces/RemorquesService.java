package com.asac.minfi.services.interfaces;

import java.util.List;

import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.Remorques;
import com.asac.minfi.entities.RemorquesResponse;

public interface RemorquesService {

	public RemorquesResponse createRemorques(Remorques remorques);
	
	public RemorquesResponse updateRemorques(Remorques remorques);
	
	public List<Remorques> listRemorques();
	
	public List<Remorques> listRemorques(String immatriculation);
	
	public RemorquesResponse listRemorquesFromMINFI(String immatriculation);

}
