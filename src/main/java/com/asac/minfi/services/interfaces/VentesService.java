package com.asac.minfi.services.interfaces;

import java.util.List;

import com.asac.minfi.entities.Ventes;
import com.asac.minfi.entities.VentesResponse;

public interface VentesService {

	public VentesResponse createVentes(Ventes ventes);
	
	public VentesResponse updateVentes(Ventes ventes);
	
	public List<Ventes> listVentes();
	
	public List<Ventes> listVentes(String immatriculation);
}
