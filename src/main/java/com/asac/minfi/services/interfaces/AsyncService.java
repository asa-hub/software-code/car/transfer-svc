package com.asac.minfi.services.interfaces;

public interface AsyncService {

	void createAssures() throws InterruptedException;
	
	void createVehicules() throws InterruptedException;
	
	void createRemorquess() throws InterruptedException;

	void updateAttestations() throws InterruptedException;

	void createVentess() throws InterruptedException;

	void createSinistres() throws InterruptedException;
}
