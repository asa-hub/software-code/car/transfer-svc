package com.asac.minfi.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.asac.minfi.services.interfaces.AsyncService;
import com.asac.minfi.utils.AppInitialiser;

@Service
public class AsyncServiceImpl implements AsyncService{
	
	@Autowired
	AppInitialiser app;
	
	public long threadSleepTime() throws InterruptedException {
		
		long start = System.currentTimeMillis();

        Thread.sleep(3000);
        
        long tSleepTime =  (System.currentTimeMillis() - start);

        System.out.println("Sleep time in ms = " + tSleepTime);
        
        return tSleepTime;
	}
	
	@Async
	@Override
	public void createAssures() throws InterruptedException{
		
		
		threadSleepTime();
		
		System.out.println("Calling createAssures service ......");
		System.out.println("Thread: " +
		          Thread.currentThread().getName());
		
		app.createAssures();
		
	}
	
	@Async
	@Override
	public void createVehicules() throws InterruptedException{
		
		threadSleepTime();
		
		System.out.println("Calling createVehicules service ......");
		System.out.println("Thread: " +
		          Thread.currentThread().getName());
		
		app.createVehiculess();
	}

	@Async
	@Override
	public void createRemorquess() throws InterruptedException {
		
		threadSleepTime();
		
		System.out.println("Calling createRemorquess service ......");
		System.out.println("Thread: " +
		          Thread.currentThread().getName());
		
		app.createRemorquess();
		
	}

	@Async
	@Override
	public void updateAttestations() throws InterruptedException {
		
		threadSleepTime();
		
		System.out.println("Calling updateAttestations service ......");
		System.out.println("Thread: " +
		          Thread.currentThread().getName());
		
		app.updateAttestations();
		
	}

	@Async
	@Override
	public void createVentess() throws InterruptedException {

		threadSleepTime();
		
		System.out.println("Calling createVentess service ......");
		System.out.println("Thread: " +
		          Thread.currentThread().getName());
		
		app.createVentess();
		
		
	}

	@Async
	@Override
	public void createSinistres() throws InterruptedException {

		threadSleepTime();
		
		System.out.println("Calling createSinistres service ......");
		System.out.println("Thread: " +
		          Thread.currentThread().getName());
		
		app.createSinistres();
		
		
	}
}
