package com.asac.minfi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.AttestationResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.entities.SinistresResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.AttestationsRepository;
import com.asac.minfi.repositories.SinistresRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.SinistresService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;

@Service
public class SinistresServiceImpl implements SinistresService{

	@Autowired
	private SinistresRepository sinistresRepository;
	
	@Autowired
	private AttestationsRepository attestationsRepository;
	
	private Logger logger = LogManager.getLogger(SinistresServiceImpl.class);



	@Override
	public SinistresResponse createSinistres(Sinistres sinistres) {
		
		logger.log(Level.INFO, "----In Sinistres Service----");

		SinistresResponse sinistresResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			
			AttestationResponse attestationResponse = attestationsRepository.listAttestationsFromMINFI(sinistres.getNumero_attestation(), accessToken);
			if(attestationResponse!=null) {
				//Si l'attestation liée existe et est vendue
				if(attestationResponse.getStatut()!=null && attestationResponse.getStatut().equalsIgnoreCase("VENDUE")) {
					
					sinistresResponse = sinistresRepository.listSinistresFromMINFI(sinistres.getNumero_attestation(), sinistres.getReference(), accessToken);
					if(sinistresResponse!=null) {
						//call put
						sinistresResponse = sinistresRepository.updateSinistre(sinistres, accessToken);
						logger.log(Level.INFO, "-----Sinistre Response returned ----");	
					}else {
						//call post
						sinistresResponse = sinistresRepository.createSinistre(sinistres, accessToken);
						logger.log(Level.INFO, "-----Sinistre Response returned ----");	
					}
					
				}else {
					logger.log(Level.INFO, "----Associated Attestation is not yet sold---");
				}
			}else {
				logger.log(Level.INFO, "----No Associated Attestation exists----");
			}

		}else {
			sinistresResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return sinistresResponse;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Sinistres> listSinistres() {
		
		return sinistresRepository.listSinistres(2);
		
	}

	@Override
	public List<Sinistres> listSinistres(String numero_attestation, String reference) {
		// TODO Auto-generated method stub
		return sinistresRepository.listSinistres(numero_attestation, reference,2);
	}

	@Override
	public SinistresResponse listSinistreFromMINFI(String numero_attestation, String reference) {
		SinistresResponse sinistresResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			sinistresResponse = sinistresRepository.listSinistresFromMINFI(numero_attestation, reference, accessToken);
		}else
			sinistresResponse = null;

		return sinistresResponse;
	}
	
	@Override
	public SinistresResponse updateSinistres(Sinistres sinistres) {

		SinistresResponse sinistresResponse = null;

		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();
		
		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			sinistresResponse = sinistresRepository.updateSinistre(sinistres, accessToken);
		}else
			sinistresResponse = null;

		return sinistresResponse;
	}

	



}
