package com.asac.minfi.services.implementations;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

import javax.security.auth.login.Configuration;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.config.java.context.JavaConfigApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.AsacMinfiApplication;
import com.asac.minfi.controllers.AssureController;
import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.AssureService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;

import javassist.bytecode.analysis.Executor;

@Service
public class AssureServiceImpl implements AssureService{

	@Autowired
	private AssureRepository assureRepository;
		
	Logger logger = LogManager.getLogger(AssureServiceImpl.class);

	
	
	@Override
	public AssureResponse createAssure(Assure assure) {  /// public CompletableFuture<AssureResponse> createAssure(Assure assure)
		
		//Service service = context.getBean(Service.class, "myService");
		
		logger.info("#######################################Starting: createAssure for assure " + assure.getNom() + " " + assure.getPrenom() );

		AssureResponse assureResponse = null;
		
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			
			assureResponse = assureRepository.listAssureFromMINFI(assure.getCode_assure().trim(), assure.getCode_assureur().trim(), accessToken);
			if(assureResponse!=null) { //Assuré found
				//call put
				assureResponse = assureRepository.updateAssure(assure, accessToken);
				logger.log(Level.INFO, "-----Assure Response returned ----");	
			}else { //Assuré does not exist
				//call post
				assureResponse = assureRepository.createAssure(assure, accessToken);
				logger.log(Level.INFO, "-----Assure Response returned ----");	
			}
		}else {
			assureResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		logger.info("#########################################Complete: createAssure for assure " + assure.getNom() + " " + assure.getPrenom());

		///return CompletableFuture.completedFuture(assureResponse);
		
		return assureResponse;
	}
	
	@Override
	public List<Assure> listAssure(){
		return assureRepository.listAssure(2);
	}

	@Override
	public List<Assure> listAssure(String code_assure, String code_assureur) {
		// TODO Auto-generated method stub
		return assureRepository.listAssure(code_assure, code_assureur, 2);
	}
	
	@Override
	public AssureResponse listAssureFromMINFI(String code_assure, String code_assureur){		

		AssureResponse assureResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			assureResponse = assureRepository.listAssureFromMINFI(code_assure, code_assureur, accessToken);
			logger.log(Level.INFO, "-----Assure Response returned ----");
		}else {
			assureResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return assureResponse;
	}

	@Override
	public AssureResponse updateAssure(Assure assure) {
		logger.log(Level.INFO, "----In Assure Service----");

		AssureResponse assureResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			assureResponse = assureRepository.updateAssure(assure, accessToken);
			logger.log(Level.INFO, "-----Assure Response returned ----");
		}else {
			assureResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return assureResponse;
	}




}
