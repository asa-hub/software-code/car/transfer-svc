package com.asac.minfi.services.implementations;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.AsacMinfiApplication;
import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.Remorques;
import com.asac.minfi.entities.RemorquesResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.repositories.RemorquesRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.RemorquesService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;

@Service
public class RemorquesServiceImpl implements RemorquesService{

	@Autowired
	private RemorquesRepository remorquesRepository;
	
	@Autowired
	private AssureRepository assureRepository;

	private Logger logger = LogManager.getLogger(RemorquesServiceImpl.class);



	@Override
	public RemorquesResponse createRemorques(Remorques remorques) {

		logger.log(Level.INFO, "----In Remorques Service----");

		RemorquesResponse remorquesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();

			///AssureResponse assureResponse = assureRepository.listAssureFromMINFI(remorques.getCode_assure(), remorques.getCode_assureur(), accessToken);
			List<Assure> assures = assureRepository.listAssure(remorques.getCode_assure().trim(), remorques.getCode_assureur().trim(), 3);

			///if(assureResponse!=null) {
			if(assures!=null && !assures.isEmpty()) {
				logger.log(Level.INFO, "-----Associated Assuré exists ----");

				remorquesResponse = remorquesRepository.listRemorquesFromMINFI(remorques.getImmatriculation_remorque().trim(), accessToken);
				
				if(remorquesResponse!=null) {
					//call put
					remorquesResponse = remorquesRepository.updateRemorques(remorques, accessToken);
					logger.log(Level.INFO, "-----Remorque Response returned ----");	

				}else {
					//call post
					remorquesResponse = remorquesRepository.createRemorques(remorques, accessToken);
					logger.log(Level.INFO, "-----Remorque Response returned ----");	
				}
			}else {
				logger.log(Level.INFO, "-----Associated Assuré does not exist ----");
			}

		}else {
			remorquesResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return remorquesResponse;
	}

	@Override
	public List<Remorques> listRemorques(){
		return remorquesRepository.listRemorques(2);
	}

	@Override
	public List<Remorques> listRemorques(String immatriculation) {
		// TODO Auto-generated method stub
		return remorquesRepository.listRemorques(immatriculation,2);
	}

	@Override
	public RemorquesResponse listRemorquesFromMINFI(String immatriculation) {
		// TODO Auto-generated method stub
		RemorquesResponse remorquesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			remorquesResponse = remorquesRepository.listRemorquesFromMINFI(immatriculation, accessToken);
		}else
			remorquesResponse = null;

		return remorquesResponse;
	}



	@Override
	public RemorquesResponse updateRemorques(Remorques remorques) {
		logger.log(Level.INFO, "----In Remorques Service----");

		RemorquesResponse remorquesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			remorquesResponse = remorquesRepository.updateRemorques(remorques, accessToken);
		}else
			remorquesResponse = null;

		return remorquesResponse;
	}



}
