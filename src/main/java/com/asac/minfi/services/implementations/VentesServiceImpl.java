package com.asac.minfi.services.implementations;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.AsacMinfiApplication;
import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AttestationResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.IntermediaireResponse;
import com.asac.minfi.entities.Ventes;
import com.asac.minfi.entities.VentesResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.repositories.AttestationsRepository;
import com.asac.minfi.repositories.IntermediaireRepository;
import com.asac.minfi.repositories.VentesRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.VentesService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;

@Service
public class VentesServiceImpl implements VentesService{

	@Autowired
	private VentesRepository ventesRepository;

	@Autowired
	private AttestationsRepository attestationsRepository;

	@Autowired
	private IntermediaireRepository intermediaireRepository;
	
	@Autowired
	private AssureRepository assureRepository;

	private Logger logger = LogManager.getLogger(VentesServiceImpl.class);



	@Override
	public VentesResponse createVentes(Ventes ventes) {

		logger.log(Level.INFO, "----In Ventes Service----");

		VentesResponse ventesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();

			List<Assure> assures = assureRepository.listAssure(ventes.getCode_assure().trim(), ventes.getCode_assureur().trim(), 3);
			if(assures!=null && !assures.isEmpty()) {

				ventesResponse = ventesRepository.listVentesFromMINFI(ventes.getNumero_attestation().trim(), ventes.getCode_assureur().trim(), accessToken);
				if(ventesResponse!=null) {
					//vente exists
					logger.log(Level.INFO, "-----Vente already exists ----");	
				}else { //vente does not exist

					//On check si l'attestation liée a été vendue
					AttestationResponse attestationResponse = attestationsRepository.listAttestationsFromMINFI(ventes.getNumero_attestation().trim(), accessToken);
					if(attestationResponse!=null) {
						if(attestationResponse.getStatut()!=null && attestationResponse.getStatut().equals("VENDUE")) {

							IntermediaireResponse intermediaireResponse = intermediaireRepository.listIntermediaireFromMINFI(ventes.getCode_intermediaire_dna().trim(), accessToken);
							if(intermediaireResponse!=null) {
								if(!intermediaireResponse.getStatut_intermediaire().equalsIgnoreCase("SUSPENDU")) {
									ventesResponse = ventesRepository.createVentes(ventes, accessToken);
									logger.log(Level.INFO, "-----Vente Response returned ----");
								}else { //si suspendu
									logger.log(Level.INFO, "-----Associated Intermediaire is suspended----");						
								}
							}else {
								logger.log(Level.INFO, "----Associated intermediaire does not exist----");
							}
						}else {
							logger.log(Level.INFO, "----Associated attestation is not yet sold----");
						}
					}else {
						logger.log(Level.INFO, "----Associated attestation does not exist----");
					}
				}
			}else {
				logger.log(Level.INFO, "-----Associated Assuré does not exist ----");
			}

		}else {
			ventesResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return ventesResponse;
	}

	@Override
	public List<Ventes> listVentes(){
		return ventesRepository.listVentes(2);
	}

	@Override
	public List<Ventes> listVentes(String immatriculation) {
		// TODO Auto-generated method stub
		return ventesRepository.listVentes(immatriculation,2);
	}

	@Override
	public VentesResponse updateVentes(Ventes ventes) {
		logger.log(Level.INFO, "----In Ventes Service----");

		VentesResponse ventesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			ventesResponse = ventesRepository.updateVentes(ventes, accessToken);
		}else
			ventesResponse = null;

		return ventesResponse;
	}



}
