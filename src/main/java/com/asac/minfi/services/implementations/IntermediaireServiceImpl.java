package com.asac.minfi.services.implementations;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.AsacMinfiApplication;
import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.Intermediaire;
import com.asac.minfi.entities.IntermediaireResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.IntermediaireRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.IntermediaireService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;

@Service
public class IntermediaireServiceImpl implements IntermediaireService{

	@Autowired
	private IntermediaireRepository intermediaireRepository;
	
	private Logger logger = LogManager.getLogger(IntermediaireServiceImpl.class);

	

	@Override
	public IntermediaireResponse createIntermediaire(Intermediaire intermediaire) {
		
		logger.log(Level.INFO, "----In Intermediaire Service----");

		IntermediaireResponse intermediaireResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			intermediaireResponse = intermediaireRepository.createIntermediaire(intermediaire, accessToken);
		}else
			intermediaireResponse = null;

		return intermediaireResponse;
	}
	
	@Override
	public List<Intermediaire> listIntermediaire(){
		return intermediaireRepository.listIntermediaire(2);
	}

	@Override
	public List<Intermediaire> listIntermediaire(String identifiant_dna) {
		// TODO Auto-generated method stub
		return intermediaireRepository.listIntermediaire(identifiant_dna,2);
	}

	@Override
	public IntermediaireResponse updateIntermediaire(Intermediaire intermediaire) {
		logger.log(Level.INFO, "----In Intermediaire Service----");

		IntermediaireResponse intermediaireResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			intermediaireResponse = intermediaireRepository.updateIntermediaire(intermediaire, accessToken);
		}else
			intermediaireResponse = null;

		return intermediaireResponse;
	}



}
