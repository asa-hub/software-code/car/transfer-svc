package com.asac.minfi.services.implementations;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.AsacMinfiApplication;
import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.RemorquesResponse;
import com.asac.minfi.entities.VehiculesResponse;
import com.asac.minfi.enums.Statut;
import com.asac.minfi.entities.Attestation;
import com.asac.minfi.entities.AttestationResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.repositories.AttestationsRepository;
import com.asac.minfi.repositories.RemorquesRepository;
import com.asac.minfi.repositories.VehiculesRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.AttestationsService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AttestationsServiceImpl implements AttestationsService{

	@Autowired
	private AttestationsRepository attestationRepository;

	@Autowired
	private VehiculesRepository vehiculesRepository;

	@Autowired
	private RemorquesRepository remorquesRepository;

	@Autowired
	private AssureRepository assureRepository;
	
	private Logger logger = LogManager.getLogger(AttestationsServiceImpl.class);



	@Override
	public AttestationResponse createAttestation(Attestation attestation) {

		logger.log(Level.INFO, "----In Attestation Service----");

		AttestationResponse attestationResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			attestationResponse = attestationRepository.createAttestation(attestation, accessToken);
		}else
			attestationResponse = null;

		return attestationResponse;
	}

	@Override
	public List<Attestation> listAttestation(){
		return attestationRepository.listAttestation(2);
	}

	@Override
	public List<Attestation> listAttestation(String numero_attestation) {
		// TODO Auto-generated method stub
		return attestationRepository.listAttestation(numero_attestation,2);
	}


	public boolean vehiculeOuRemorqueExists(String remorque, String immatriculation, String accessToken) {

		//On vérifie maintenant que le vehicule ou la remorque associé à cette attestation existe vraiment
		boolean exists = false;

		String identifiantVehOuRem = remorque;
		System.out.println("----identifiantVehOuRem----" + identifiantVehOuRem);
		logger.log(Level.INFO, "----identifiantVehOuRem----" + identifiantVehOuRem);

		if(identifiantVehOuRem!=null) {
			System.out.println("----identifiantVehOuRem--------" + identifiantVehOuRem);

			if(identifiantVehOuRem.trim().equals("0")) { //vehicule
				VehiculesResponse vehiculesResponse = vehiculesRepository.listVehiculeFromMINFI(immatriculation, accessToken);
				System.out.println("----vehiculesResponse got--------" );

				if(vehiculesResponse!=null) {
					System.out.println("----vehicule exists--------" );
					exists = true;
				}else { //si le véhicule n'existe pas, essayons de voir si c'est une remorque
					RemorquesResponse remorquesResponse = remorquesRepository.listRemorquesFromMINFI(immatriculation, accessToken);
					System.out.println("----remorquesResponse got--------" );
					if(remorquesResponse!=null) {
						System.out.println("----remorque exists--------" );
						exists = true;
					}
				}
			}else if(identifiantVehOuRem.trim().equals("1")) { //remorque
				RemorquesResponse remorquesResponse = remorquesRepository.listRemorquesFromMINFI(immatriculation, accessToken);
				System.out.println("----remorquesResponse got--------" );

				if(remorquesResponse!=null) {
					System.out.println("----remorque exists--------" );
					exists = true;
				}else {
					//si la remorque n'existe pas, essayons de voir si c'est un véhicule
					VehiculesResponse vehiculesResponse = vehiculesRepository.listVehiculeFromMINFI(immatriculation, accessToken);
					System.out.println("----vehiculesResponse got--------" );
					if(vehiculesResponse!=null) {
						System.out.println("----vehicule exists--------" );
						exists = true;
					}
				}
			}
		}else {
			System.out.println("----identifiantVehOuRem is null--------" );
			logger.log(Level.INFO, "----identifiantVehOuRem is null--------");
		}

		return exists;
	}

	@Override
	public AttestationResponse updateAttestation(Attestation attestation) {
		logger.log(Level.INFO, "----In Attestation Service----");

		AttestationResponse attestationResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {

			List<Assure> assures = assureRepository.listAssure(attestation.getCode_assure().trim(), attestation.getCode_assureur().trim(), 3);
			if(assures!=null && !assures.isEmpty()) {
				String accessToken = authResp.getAccess_token();
				System.out.println("----accessToken------------");
				attestationResponse = attestationRepository.listAttestationsFromMINFI(attestation.getNumero_attestation().trim(), accessToken);
				if(attestationResponse!=null) { //l'attestation existe
					//si l'attestation est toujours en stock
					if(attestationResponse.getStatut()!=null && attestationResponse.getStatut().equalsIgnoreCase(Statut.EN_STOCK.value())) { //on doit aussi check si c'est MISE A REBUS et le nouveau statut c'est VENDUE, la ça passe ; si c'est déjà vendue et que le nouveau statut veut passer à ANNULER on valide

						//rassurons nous que nous sommes entrain de passer l'attestation à Vendue ou Mise au rebut
						if(attestation.getStatut().equals(Statut.VENDUE.value()) || attestation.getStatut().equals(Statut.MISE_AU_REBUT.value())) {

							System.out.println("----Statut--------" + attestationResponse.getStatut());

							//On vérifie maintenant que le vehicule ou la remorque associé à cette attestation existe vraiment
							//boolean exists = vehiculeOuRemorqueExists(attestation.getRemorque(), attestation.getImmatriculation(), accessToken);

							//if(exists) { // si le vehicule ou la remorque existe

							///////////////////on ne fait plus la vérification de l'existence du véhicule ou de la remorque car le champ remorque n'est pas requis pour les attestations en stock////////////////////////////
							attestationResponse = attestationRepository.updateAttestation(attestation, accessToken); 
							logger.log(Level.INFO, "-----Attestation Response returned ----");	
							//}else {
							//	logger.log(Level.INFO, "-----Véhicule or Remorque does not exist -------");
							//}
						}else if(attestation.getStatut().equals(Statut.REMISE_EN_VIGUEUR.value())) {

							//Il faut un service côté MINFI qui permet de rechercher une autre attestation qui a le statut SUSPENDUE, le même numéro de police et même immatriculation
							//Une fois cette attestation retrouvée, alors on met à jour le statut de l'attestation courante par REMISE EN VIGUEUR

						}else {
							logger.log(Level.INFO, "-----Le statut de l'attestation en base est " + attestationResponse.getStatut().trim() + " mais le statut de l'attestation pour la mise à jour est  " + attestation.getStatut().trim() + " au lieu de " + Statut.VENDUE.value() + ", " + Statut.MISE_AU_REBUT.value() + " ou " + Statut.REMISE_EN_VIGUEUR.value() + "------");
						}
					}else { //l'attestation est déjà vendue, dans un autre statut, ou n'existe pas
						logger.log(Level.INFO, "---Attestation Status----" + attestation.getStatut().trim());
						logger.log(Level.INFO, "-----Attestation not in stock or status is null ----");

						if(attestationResponse.getStatut().equals(Statut.VENDUE.value())) {

							if(attestation.getStatut().equals(Statut.ANNULEE.value()) || attestation.getStatut().equals(Statut.RESILIEE.value()) || attestation.getStatut().equals(Statut.SUSPENDUE.value())) {

								boolean exists = vehiculeOuRemorqueExists(attestation.getRemorque(), attestation.getImmatriculation(), accessToken);

								if(exists) { // si le vehicule ou la remorque existe
									attestationResponse = attestationRepository.updateAttestation(attestation, accessToken);
									logger.log(Level.INFO, "-----Attestation Response returned ----");	
								}else {
									logger.log(Level.INFO, "-----Véhicule or Remorque does not exist -------");
								}

							}else {
								logger.log(Level.INFO, "-----Le statut de l'attestation en base est " + attestationResponse.getStatut().trim() + " mais le statut de l'attestation pour la mise à jour est  " + attestation.getStatut().trim() + " au lieu de " + Statut.ANNULEE.value() + ", " + Statut.RESILIEE.value() + " ou " + Statut.SUSPENDUE.value() + "------");	
							}
						}else if(attestationResponse.getStatut().equals(Statut.SUSPENDUE.value())) {
							if(attestation.getStatut().equals(Statut.RESILIEE.value())) {
								boolean exists = vehiculeOuRemorqueExists(attestation.getRemorque(), attestation.getImmatriculation(), accessToken);

								if(exists) { // si le vehicule ou la remorque existe
									attestationResponse = attestationRepository.updateAttestation(attestation, accessToken);
									logger.log(Level.INFO, "-----Attestation Response returned ----");	
								}else {
									logger.log(Level.INFO, "-----Véhicule or Remorque does not exist -------");
								}
							}else {
								logger.log(Level.INFO, "-----Le statut de l'attestation en base est " + attestationResponse.getStatut().trim() + " mais le statut de l'attestation pour la mise à jour est  " + attestation.getStatut().trim() + " au lieu de " + Statut.RESILIEE.value() + "------");	
							}
						}else if(attestationResponse.getStatut().equals(Statut.REMISE_EN_VIGUEUR.value())) {

							if(attestation.getStatut().equals(Statut.ANNULEE.value()) || attestation.getStatut().equals(Statut.RESILIEE.value()) || attestation.getStatut().equals(Statut.SUSPENDUE.value())) {
								boolean exists = vehiculeOuRemorqueExists(attestation.getRemorque(), attestation.getImmatriculation(), accessToken);

								if(exists) { // si le vehicule ou la remorque existe
									attestationResponse = attestationRepository.updateAttestation(attestation, accessToken);
									logger.log(Level.INFO, "-----Attestation Response returned ----");	
								}else {
									logger.log(Level.INFO, "-----Véhicule or Remorque does not exist -------");
								}
							}else {
								logger.log(Level.INFO, "-----Le statut de l'attestation en base est " + attestationResponse.getStatut().trim() + " mais le statut de l'attestation pour la mise à jour est  " + attestation.getStatut().trim() + " au lieu de " + Statut.ANNULEE.value() + ", " + Statut.RESILIEE.value() + " ou " + Statut.SUSPENDUE.value() + "------");	
							}
						}

					}
				}else {
					logger.log(Level.INFO, "-----Attestation does not exists ----");	
				}

			}else {
				logger.log(Level.INFO, "-----Associated Assuré does not exist ----");
			}
		}else {
			attestationResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return attestationResponse;
	}



}
