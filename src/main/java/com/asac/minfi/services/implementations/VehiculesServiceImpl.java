package com.asac.minfi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.Vehicules;
import com.asac.minfi.entities.VehiculesResponse;
import com.asac.minfi.repositories.AdressesRepository;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.repositories.VehiculesRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.VehiculesService;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;

@Service
public class VehiculesServiceImpl implements VehiculesService{

	@Autowired
	private VehiculesRepository vehiculesRepository;

	@Autowired
	private AssureRepository assureRepository;

	private Logger logger = LogManager.getLogger(VehiculesServiceImpl.class);


	@Override
	public VehiculesResponse createVehicules(Vehicules vehicules) {

		logger.log(Level.INFO, "----In Vehicules Service----");

		VehiculesResponse vehiculesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();

			//On vérifie d'abord que l'assuré lié au véhicule existe

			///AssureResponse assureResponse = assureRepository.listAssureFromMINFI(vehicules.getCode_assure(), vehicules.getCode_assureur(), accessToken);
			List<Assure> assures = assureRepository.listAssure(vehicules.getCode_assure().trim(), vehicules.getCode_assureur().trim(), 3);
			if(assures!=null && !assures.isEmpty()) {
			///if(assureResponse!=null) {
				logger.log(Level.INFO, "-----Associated Assuré exists with name----" + assures.get(0).getNom() + " and size " + assures.size());
				///logger.log(Level.INFO, "-----Associated Assuré exists with name----" + assureResponse.getNom());

				vehiculesResponse = vehiculesRepository.listVehiculeFromMINFI(vehicules.getImmatriculation(), accessToken);
				if(vehiculesResponse!=null) {
					//call put
					vehiculesResponse = vehiculesRepository.updateVehicule(vehicules, accessToken);
					logger.log(Level.INFO, "-----Vehicule Response returned ----");	

				}else {
					vehiculesResponse = vehiculesRepository.createVehicule(vehicules, accessToken);
					logger.log(Level.INFO, "-----Vehicule Response returned ----");	
				}
			}else {
				logger.log(Level.INFO, "-----Associated Assuré does not exist ----");
			}

		}else {
			vehiculesResponse = null;
			logger.log(Level.WARN, "-----Auth Response returned null ---");
		}

		return vehiculesResponse;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Vehicules> listVehicules() {

		return vehiculesRepository.listVehicules(2);

	}

	@Override
	public List<Vehicules> listVehicules(String immatriculation) {
		// TODO Auto-generated method stub
		return vehiculesRepository.listVehicules(immatriculation,2);
	}

	@Override
	public VehiculesResponse updateVehicules(Vehicules vehicules) {

		VehiculesResponse vehiculesResponse = null;

		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			vehiculesResponse = vehiculesRepository.updateVehicule(vehicules, accessToken);
		}else
			vehiculesResponse = null;

		return vehiculesResponse;
	}

	@Override
	public VehiculesResponse listVehiculeFromMINFI(String immatriculation) {

		VehiculesResponse vehiculesResponse = null;
		//Insertion dans la BD du MINFI via appel de leurs WS
		//Authentification
		AuthentificationResponse authResp = ASACMinfiUtils.getAuthentified();

		if(authResp!=null && authResp.getAccess_token()!=null ) {
			String accessToken = authResp.getAccess_token();
			vehiculesResponse = vehiculesRepository.listVehiculeFromMINFI(immatriculation, accessToken);
		}else
			vehiculesResponse = null;

		return vehiculesResponse;
	}



}
