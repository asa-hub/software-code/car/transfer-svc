package com.asac.minfi.repositories;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.ErrorResponse;
import com.asac.minfi.entities.ErrorResponse2;
import com.asac.minfi.entities.Remorques;
import com.asac.minfi.entities.RemorquesResponse;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

@Repository
public class RemorquesRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String urlBase = "";

	@Autowired
	private Environment env;

	private Logger logger = LogManager.getLogger(RemorquesRepository.class);



	public RemorquesRepository() {
		super();
	}

	public RemorquesResponse createRemorques (Remorques remorques, String accessToken) {
		logger.log(Level.INFO, "--About to create remorques---");

		urlBase = env.getProperty("api.dna.remorque");

		RemorquesResponse remorquesResponse = null;

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPost postRequest = new HttpPost(queryURL);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(remorques);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			postRequest.setEntity(entity);



			logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(postRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {
					remorquesResponse = objectMapper.readValue(result, RemorquesResponse.class);

					logger.log(Level.INFO, "----remorques response-----" + remorquesResponse.getImmatriculation_remorque());

				}catch(UnrecognizedPropertyException upe) {

					ErrorResponse errorResponse;
					ErrorResponse2 errorResponse2;

					logger.log(Level.ERROR, "----Unrecognized property exception caught ------");

					try {

						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();

					}catch(UnrecognizedPropertyException upe2) {

						errorResponse2 = objectMapper.readValue(result, ErrorResponse2.class);
						description = errorResponse2.getViolations().get(0).getField() + " : " + errorResponse2.getViolations().get(0).getMessage();
					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, 4, "TRANS_REM_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----Remorque not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "ERROR");


					return null;

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, 3, "TRANS_REM_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----Remorque updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, -3, "TRANS_REM_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, 4, "TRANS_REM_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "ERROR");

				}


			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}


		} catch (Exception e2) {
			e2.printStackTrace();
		}

		if(remorquesResponse!=null)
			logger.log(Level.INFO, "*****Remorques Response*****" + remorquesResponse.toString());

		return remorquesResponse;

	}


	@SuppressWarnings("deprecation")
	public List<Remorques> listRemorques(int status) {

		String remorques = "select * from remorque where c_status = ?";

		logger.log(Level.INFO, "----Selecting from remorques----");

		List<Remorques> listAss = jdbcTemplate.query(remorques, new Object[]{status},(rs,rowNum) ->{
			Remorques a = new Remorques();
			a.setRid(rs.getInt("rid"));
			a.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			a.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			a.setImmatriculation_remorque(rs.getString("immatriculation_remorque"));

			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------remorques size-----" + listAss.size());

		return listAss;

	}

	@SuppressWarnings("deprecation")
	public List<Remorques> listRemorques(String immatriculation_remorque, int status) {

		logger.log(Level.INFO, "---immatriculation_remorque---" + immatriculation_remorque );

		String remorques = "select * from remorque where immatriculation_remorque = ? and c_status = ? ";

		logger.log(Level.INFO, "----Selecting from remorques----");

		List<Remorques> listAss = jdbcTemplate.query(remorques, new Object[]{immatriculation_remorque,status},(rs,rowNum) ->{
			Remorques a = new Remorques();
			a.setRid(rs.getInt("rid"));
			a.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			a.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			a.setImmatriculation_remorque(rs.getString("immatriculation_remorque"));

			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------remorques size-----" + listAss.size());

		return listAss;

	}

	public RemorquesResponse listRemorquesFromMINFI(String immatriculation, String accessToken){

		logger.log(Level.INFO, "--About to list remorques---");

		urlBase = env.getProperty("api.dna.remorque");

		RemorquesResponse remorquesResponse = null;

		String queryURL = urlBase+"/"+immatriculation;
		logger.log(Level.INFO, "---queryURL---" + queryURL);

		DefaultHttpClient httpclient;

		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpGet getRequest = new HttpGet(queryURL);
			getRequest.setHeader("Content-type", "application/json");
			getRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + getRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + getRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();


			try {
				HttpResponse httpResponse = httpclient.execute(getRequest);
				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);


				if (entity2 != null && entity2.getContentType()!=null) {
					// return it as a String
					String result = EntityUtils.toString(entity2);
					entity2.getContent().close();

					logger.log(Level.INFO, "-----result-----"+ result);
					remorquesResponse = objectMapper.readValue(result, RemorquesResponse.class);

					logger.log(Level.INFO, "----remorque response N° Immatriculation-----" + remorquesResponse.getImmatriculation_remorque());

				}else {
					logger.log(Level.INFO, "-----Entity Content Type Null-----");
				}

			}catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}


		}catch (Exception e2) {
			e2.printStackTrace();
		}


		return remorquesResponse;

	}


	public RemorquesResponse updateRemorques (Remorques remorques, String accessToken) {
		logger.log(Level.INFO, "--About to update remorques---");

		urlBase = env.getProperty("api.dna.remorque");


		RemorquesResponse remorquesResponse = null;

		//String queryURL = "https://dna-core.sprint-pay.com/api/remorques";
		String queryURL = urlBase;

		/*restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken); //"Bearer " +*/

		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPut putRequest = new HttpPut(queryURL);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + putRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + putRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(remorques);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			putRequest.setEntity(entity);



			logger.log(Level.INFO, "-----putRequest.getEntity().toString()-----" + putRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + putRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + putRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(putRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {
					remorquesResponse = objectMapper.readValue(result, RemorquesResponse.class);
					logger.log(Level.INFO, "----UPDATE: remorques response-----" + remorquesResponse.getImmatriculation_remorque());
					
				}catch(UnrecognizedPropertyException upe) {


					ErrorResponse errorResponse;
					ErrorResponse2 errorResponse2;

					logger.log(Level.ERROR, "----Unrecognized property exception caught ------");

					try {

						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();

					}catch(UnrecognizedPropertyException upe2) {

						errorResponse2 = objectMapper.readValue(result, ErrorResponse2.class);
						description = errorResponse2.getViolations().get(0).getField() + " : " + errorResponse2.getViolations().get(0).getMessage();
					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, 4, "TRANS_REM_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----UPDATE: Remorque not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "ERROR");


					return null;

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, 3, "TRANS_REM_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----UPDATE: Remorque updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, -3, "TRANS_REM_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					remorques = updateRemorquesInOurBD(remorques, 4, "TRANS_REM_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(remorques, description, "ERROR");

				}

				logger.log(Level.INFO, "----remorques response-----" + remorquesResponse.getImmatriculation_remorque());


			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(); 
			}

		
		} catch (Exception e2) {
			e2.printStackTrace();
		}



		if(remorquesResponse!=null)
			logger.log(Level.INFO, "*****Remorques Response*****" + remorquesResponse.toString());

		return remorquesResponse;

	}


	public Remorques updateRemorquesInOurBD(Remorques remorques, int status_code, String comment) {
		
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date tr_date = dateFormat.parse(strDate);

			String update_assure = "update remorque set c_status = ?, c_date_transfer = ?, commentaires = ? where immatriculation_remorque = ? ";

			System.out.println("----Immatriculation remorque--------------" + remorques.getImmatriculation_remorque() + "---length---" + remorques.getImmatriculation_remorque().length());
			Object[] params = new Object[] {status_code, tr_date, comment, remorques.getImmatriculation_remorque()};

			int update =  jdbcTemplate.update(update_assure, params);

			logger.log(Level.INFO, "---update---" + update);

			if(update!=1) {
				return null;
			}

			remorques = listRemorques(remorques.getImmatriculation_remorque(), status_code).get(0);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return remorques;

	}

	public Boolean insertIntoJournal(Remorques remorques, String description, String status) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date dateInscription = dateFormat.parse(strDate);

			//String insertIntoJournal = "Insert into journal (reference, description, status, date_inscription) values (?, ?, ?, ?)";

			String insertIntoJournal = "INSERT INTO journal_remorque (code_assure, code_assureur, immatriculation_remorque, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires, j_logdate) " +
					"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";


			//String updateJournal = "Update journal set description = ?, status = ?, date_inscription = ? where reference = ?";



			try {

				//Object[] params = new Object[] {"TRANSREM"+String.valueOf(remorques.getRid()), description, status, dateInscription};

				Object[] params = new Object[] {
						remorques.getCode_assure(), 
						remorques.getCode_assureur(), 
						remorques.getImmatriculation_remorque(), 
						remorques.getC_status(), 
						remorques.getC_date_creation(), 
						remorques.getC_date_mis_a_jour()!=null?remorques.getC_date_mis_a_jour():null, 
						remorques.getC_date_transfer(), 
						remorques.getCommentaires(), 
						dateInscription};

				int insert =  jdbcTemplate.update(insertIntoJournal, params);

				logger.log(Level.INFO, "---insert---" + insert);

				if(insert!=1) {
					return Boolean.FALSE;
				}

			}catch(DuplicateKeyException dke) {

				/**Object[] params = new Object[] {description, status, dateInscription,String.valueOf(assure.getAid())};

				int update =  jdbcTemplate.update(updateJournal, params);

				logger.log(Level.INFO, "---update---" + update);

				if(update!=1) {
					return Boolean.FALSE;
				}*/
				dke.printStackTrace();

			}

		}catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Boolean.TRUE;

	}



}
