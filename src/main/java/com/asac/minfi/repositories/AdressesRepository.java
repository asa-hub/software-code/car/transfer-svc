package com.asac.minfi.repositories;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class AdressesRepository { ////////////////NOT USED//////////////////////////////

	@Autowired(required = false)
	private JdbcTemplate jdbcTemplate;

	@Autowired(required = false)
	private RestTemplate restTemplate;


	public AuthentificationResponse getAuthentified() {

		
		AuthentificationResponse authResp = null;

		String queryAuthURL = "https://dna-uaa.sprint-pay.com/oauth/token";
		restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.setBasicAuth("Basic d2ViX2FwcDpBSkRLREhKS0hES1NKS0RKSERKR01RU0lPVUpES1NKRE1MQERLSlNES0FLREhTSkRIU0RMTlNOSktIRCNeQERLS1NES0xTREtKU0xLREpLU0pES0xTSkRLU0pES1NKREtTSkQ=");

		try {

			JSONObject bodyJsonObject = new JSONObject();
			bodyJsonObject.put("grant_type", "password");
			bodyJsonObject.put("username", "survie");
			bodyJsonObject.put("password", "survie123");
			bodyJsonObject.put("scope", "openid");

			HttpEntity<String> request = new HttpEntity<String>(bodyJsonObject.toString(), headers);
			authResp = restTemplate.postForObject(queryAuthURL, request, AuthentificationResponse.class);

		}catch(JSONException je) {
			je.printStackTrace();
		}

		return authResp;

	}

	public AdressesResponse createAdresse(Adresses adresses, String accessToken) {
		AdressesResponse adressesResponse = null;
		
		String queryAuthURL = "https://dna-core.sprint-pay.com/api/adresses";
		restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBasicAuth("Bearer " + accessToken);

		try {
			
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;
			
			jsonStr = objectMapper.writeValueAsString(adresses);
			

			HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
			
			//catch the exception for the case where the access token has expired
			adressesResponse = restTemplate.postForObject(queryAuthURL, request, AdressesResponse.class);

		}catch(JsonProcessingException je) {
			je.printStackTrace();
		}
		
		return adressesResponse;
	}

}
