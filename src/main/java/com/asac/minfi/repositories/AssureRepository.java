package com.asac.minfi.repositories;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.asac.minfi.controllers.AssureController;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.ErrorResponse;
import com.asac.minfi.entities.ErrorResponse2;
import com.asac.minfi.entities.Journal;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.asac.minfi.utils.AppInitialiser;
import com.asac.minfi.utils.Configurations;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

@Repository
public class AssureRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	//@Autowired
	//private Environment env;

	//private ASACMinfiUtils asacMinfiUtils;

	private String urlBase = "";

	private Logger logger = LogManager.getLogger(AssureRepository.class);

	@Autowired
	private Environment env;

	public AssureRepository() {
		super();
		//asacMinfiUtils = new ASACMinfiUtils();
	}

	public AssureResponse createAssure (Assure assure, String accessToken) {

		logger.log(Level.INFO, "--------About to create assure -------");

		urlBase = env.getProperty("api.dna.assure");

		AssureResponse assureResponse = null;
		logger.log(Level.INFO, "---urlBase: " + urlBase);

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPost postRequest = new HttpPost(queryURL);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("Authorization", "Bearer " + accessToken);

			//logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
			//logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));


			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;
			jsonStr = objectMapper.writeValueAsString(assure);
			//logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);
			postRequest.setEntity(entity);


			//logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
			//logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
			//logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(postRequest);

				//logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				//logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				//logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----entity contentType-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****response status line******* : " + httpResponse.getStatusLine().toString());
				logger.log(Level.INFO, "---response status line reason---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO,"----response status code---" +httpResponse.getStatusLine().getStatusCode());
				//logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());


				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				//logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {

					assureResponse = objectMapper.readValue(result, AssureResponse.class);
					logger.log(Level.INFO, "----assure response N° Contribuable-----" + assureResponse.getNum_contribuable());

				}catch(UnrecognizedPropertyException upe) {

					ErrorResponse errorResponse;
					ErrorResponse2 errorResponse2;

					logger.log(Level.ERROR, "----Unrecognized property exception caught ------");

					try {

						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();

					}catch(UnrecognizedPropertyException upe2) {

						errorResponse2 = objectMapper.readValue(result, ErrorResponse2.class);
						description = errorResponse2.getViolations().get(0).getField() + " : " + errorResponse2.getViolations().get(0).getMessage();
					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, 4, "TRANS_ASS_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "-------Assure not updated on our side --------");

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "ERROR");


					return null;

				}

				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, 3, "TRANS_ASS_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "-------Assure updated on our side ---------");

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, -3, "TRANS_ASS_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "PENDING TRANSFER"); //SYSTEM ERROR


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, 4, "TRANS_ASS_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "ERROR"); //REJECTED BY MINFI SERVER

				}

			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}


		if(assureResponse!=null)
			logger.log(Level.INFO, "*******Assure Response successfully returned ********" );

		return assureResponse;

	}


	@SuppressWarnings("deprecation")
	public List<Assure> listAssure(int status) {

		String assure = "select * from assure where c_status = ?";

		logger.log(Level.INFO, "----Selecting from assure----");

		List<Assure> listAss = jdbcTemplate.query(assure, new Object[]{status},(rs,rowNum) ->{
			Assure a = new Assure();
			a.setAid(rs.getInt("aid"));
			a.setCategorie_permis(StringUtils.trimWhitespace(rs.getString("categorie_permis").trim()));
			a.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			a.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			a.setBoite_postale(StringUtils.trimWhitespace(rs.getString("boite_postale").trim()));
			a.setDate_delivrance(rs.getString("date_delivrance"));
			a.setDate_naissance(rs.getString("date_naissance"));
			a.setDate_naissance_conducteur(rs.getString("date_naissance_conducteur"));
			a.setEmail(StringUtils.trimWhitespace(rs.getString("email").trim()));
			a.setNom(StringUtils.trimWhitespace(rs.getString("nom").trim()));
			a.setNom_prenom_conducteur(StringUtils.trimWhitespace(rs.getString("nom_prenom_conducteur").trim()));
			a.setNum_contribuable(StringUtils.trimWhitespace(rs.getString("num_contribuable").trim()));
			a.setNumero_permis(StringUtils.trimWhitespace(rs.getString("numero_permis").trim()));
			a.setPermis_delivre_par(StringUtils.trimWhitespace(rs.getString("permis_delivre_par").trim()));
			a.setPrenom(StringUtils.trimWhitespace(rs.getString("prenom").trim()));
			a.setProfession(StringUtils.trimWhitespace(rs.getString("profession").trim()));
			a.setQualite(StringUtils.trimWhitespace(rs.getString("qualite").trim()));
			a.setRue(StringUtils.trimWhitespace(rs.getString("rue").trim()));
			a.setTelephone(StringUtils.trimWhitespace(rs.getString("telephone").trim()));
			a.setVille(StringUtils.trimWhitespace(rs.getString("ville").trim()));

			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------assure size-----" + listAss.size());

		return listAss;

	}

	@SuppressWarnings("deprecation")
	public List<Assure> listAssure(String code_assure, String code_assureur, int status) {
		logger.log(Level.INFO, "---code_assure---" + code_assure + " ------code_assureur-----" + code_assureur);

		String assure = "select * from assure where code_assure = ? and code_assureur = ? and c_status = ?";

		logger.log(Level.INFO, "----Selecting from assure----");

		List<Assure> listAss = jdbcTemplate.query(assure, new Object[]{code_assure, code_assureur, status},(rs,rowNum) ->{
			Assure a = new Assure();
			a.setAid(rs.getInt("aid"));
			a.setCategorie_permis(StringUtils.trimWhitespace(rs.getString("categorie_permis").trim()));
			a.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			a.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			a.setBoite_postale(StringUtils.trimWhitespace(rs.getString("boite_postale").trim()));
			a.setDate_delivrance(rs.getString("date_delivrance"));
			a.setDate_naissance(rs.getString("date_naissance"));
			a.setDate_naissance_conducteur(rs.getString("date_naissance_conducteur"));
			a.setEmail(StringUtils.trimWhitespace(rs.getString("email").trim()));
			a.setNom(StringUtils.trimWhitespace(rs.getString("nom").trim()));
			a.setNom_prenom_conducteur(StringUtils.trimWhitespace(rs.getString("nom_prenom_conducteur").trim()));
			a.setNum_contribuable(StringUtils.trimWhitespace(rs.getString("num_contribuable").trim()));
			a.setNumero_permis(StringUtils.trimWhitespace(rs.getString("numero_permis").trim()));
			a.setPermis_delivre_par(StringUtils.trimWhitespace(rs.getString("permis_delivre_par").trim()));
			a.setPrenom(StringUtils.trimWhitespace(rs.getString("prenom").trim()));
			a.setProfession(StringUtils.trimWhitespace(rs.getString("profession").trim()));
			a.setQualite(StringUtils.trimWhitespace(rs.getString("qualite").trim()));
			a.setRue(StringUtils.trimWhitespace(rs.getString("rue").trim()));
			a.setTelephone(StringUtils.trimWhitespace(rs.getString("telephone").trim()));
			a.setVille(StringUtils.trimWhitespace(rs.getString("ville").trim()));

			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------assure size-----" + listAss.size());

		return listAss;

	}


	public AssureResponse listAssureFromMINFI(String code_assure, String code_assureur, String accessToken){

		logger.log(Level.INFO, "--About to list assure---");

		urlBase = env.getProperty("api.dna.assure");


		AssureResponse assureResponse = null;

		String queryURL = urlBase+"/"+code_assure.trim()+"/"+code_assureur.trim();
		//logger.log(Level.INFO, "---queryURL---" + queryURL);

		DefaultHttpClient httpclient;

		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpGet getRequest = new HttpGet(queryURL);
			getRequest.setHeader("Content-type", "application/json");
			getRequest.setHeader("Authorization", "Bearer " + accessToken);

			//logger.log(Level.INFO, "---Content-type----" + getRequest.getHeaders("Content-type"));
			//logger.log(Level.INFO, "----Authorization---" + getRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();


			try {
				HttpResponse httpResponse = httpclient.execute(getRequest);
				//logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				//logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				//logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----entity contentType-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****response status line******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----response status line reason---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----response status code---" +httpResponse.getStatusLine().getStatusCode());
				//logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				//logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				//logger.log(Level.INFO, "****entity2******* : " + entity2);


				if (entity2 != null && entity2.getContentType()!=null) {

					String result = EntityUtils.toString(entity2);
					entity2.getContent().close();

					//logger.log(Level.INFO, "-----result-----"+ result);
					assureResponse = objectMapper.readValue(result, AssureResponse.class);

					logger.log(Level.INFO, "----assure response N° Contribuable-----" + assureResponse.getNum_contribuable());

				}else {
					logger.log(Level.INFO, "-----Entity Content Type Null-----");
				}

			}catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		}catch (Exception e2) {
			e2.printStackTrace();
		}


		return assureResponse;

	}


	public AssureResponse updateAssure(Assure assure, String accessToken) {
		logger.log(Level.INFO, "------About to update assure-------");

		urlBase = env.getProperty("api.dna.assure");


		AssureResponse assureResponse = null;

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPut putRequest = new HttpPut(queryURL);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("Authorization", "Bearer " + accessToken);

			//logger.log(Level.INFO, "---Content-type----" + putRequest.getHeaders("Content-type"));
			//logger.log(Level.INFO, "----Authorization---" + putRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(assure);
			//logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			putRequest.setEntity(entity);



			//logger.log(Level.INFO, "-----putRequest.getEntity().toString()-----" + putRequest.getEntity().toString());
			//logger.log(Level.INFO, "using getRequestLine(): " + putRequest.getRequestLine());
			//logger.log(Level.INFO, "using getURI(): " + putRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(putRequest);

				//logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				//logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				//logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----entity content type-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****responses status line******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "---responses status line reason---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----response status code---" +httpResponse.getStatusLine().getStatusCode());
				//logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				//logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				//logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				//logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {

					assureResponse = objectMapper.readValue(result, AssureResponse.class);
					logger.log(Level.INFO, "-------UPDATE: assure response N° Contribuable-------" + assureResponse.getNum_contribuable());

				}catch(UnrecognizedPropertyException upe) {


					ErrorResponse errorResponse;
					ErrorResponse2 errorResponse2;

					logger.log(Level.ERROR, "--------Unrecognized property exception caught ------");

					try {

						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();

					}catch(UnrecognizedPropertyException upe2) {

						errorResponse2 = objectMapper.readValue(result, ErrorResponse2.class);
						description = errorResponse2.getViolations().get(0).getField() + " : " + errorResponse2.getViolations().get(0).getMessage();
					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, 4, "TRANS_ASS_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "---------UPDATE: Assure not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "ERROR");


					return null;

				}


				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, 3, "TRANS_ASS_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "--------UPDATE: Assure updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, -3, "TRANS_ASSS_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "PENDING TRANSFER"); //SYSTEM ERROR


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					assure = updateAssureInOurBD(assure, 4, "TRANS_ASS_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(assure, description, "ERROR"); //REJECTED BY MINFI SERVER

				}


			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(); 
			}

		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}


		if(assureResponse!=null)
			logger.log(Level.INFO, "*****Assure Response successfully returned*****");

		return assureResponse;

	}

	public Assure updateAssureInOurBD(Assure assure, int status_code, String comment) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date tr_date = dateFormat.parse(strDate);

			String update_assure = "update assure set c_status = ?, c_date_transfer = ?, commentaires = ? where code_assure = ? and code_assureur = ? ";

			Object[] params = new Object[] {status_code, tr_date, comment, assure.getCode_assure(), assure.getCode_assureur()};

			int update =  jdbcTemplate.update(update_assure, params);

			logger.log(Level.INFO, "---update---" + update);

			if(update!=1) {
				return null;
			}

			assure = listAssure(assure.getCode_assure(), assure.getCode_assureur(), status_code).get(0);
			logger.log(Level.INFO, "---assure-----" + assure.getC_date_transfer() + assure.getC_status());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return assure;

	}

	public Boolean insertIntoJournal(Assure assure, String description, String status) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {

			Date dateInscription = dateFormat.parse(strDate);

			//String insertIntoJournal = "Insert into journal (reference, description, status, date_inscription) values (?, ?, ?, ?)";
			String insertIntoJournal = "INSERT INTO journal_assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, "+
					"numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires, j_logdate) "+
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "+
					"?, ?,?, ?, ?, ?,?, ?,?, ?, ?, ?, ? );";

			//String updateJournal = "Update journal set description = ?, status = ?, date_inscription = ? where reference = ?";


			try {

				//Object[] params = new Object[] {"TRANSASS"+String.valueOf(assure.getAid()), description, status, dateInscription};
				Object[] params = new Object[] {assure.getCode_assure(), assure.getCode_assureur(), assure.getQualite(), assure.getNom(), assure.getPrenom(), assure.getProfession(),assure.getDate_naissance()!=null?sdf.parse(assure.getDate_naissance()):null, assure.getNum_contribuable(), assure.getVille(), assure.getRue(), assure.getBoite_postale(), assure.getTelephone(), assure.getEmail(), 
						assure.getNumero_permis(), assure.getCategorie_permis(), assure.getDate_delivrance()!=null?sdf.parse(assure.getDate_delivrance()):null, assure.getPermis_delivre_par(), assure.getNom_prenom_conducteur(), assure.getDate_naissance_conducteur()!=null?sdf.parse(assure.getDate_naissance_conducteur()):null, assure.getC_status(), assure.getC_date_creation(), assure.getC_date_mis_a_jour(), assure.getC_date_transfer(), assure.getCommentaires(), dateInscription};

				int insert =  jdbcTemplate.update(insertIntoJournal, params);

				logger.log(Level.INFO, "---insert---" + insert);

				if(insert!=1) {
					return Boolean.FALSE;
				}

			}catch(DuplicateKeyException dke) {

				/**Object[] params = new Object[] {description, status, dateInscription,String.valueOf(assure.getAid())};

				int update =  jdbcTemplate.update(updateJournal, params);

				logger.log(Level.INFO, "---update---" + update);

				if(update!=1) {
					return Boolean.FALSE;
				}*/
				dke.printStackTrace();

			}

		}catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Boolean.TRUE;

	}

}
