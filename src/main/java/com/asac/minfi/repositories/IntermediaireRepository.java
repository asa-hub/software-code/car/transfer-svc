package com.asac.minfi.repositories;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.ErrorResponse;
import com.asac.minfi.entities.Intermediaire;
import com.asac.minfi.entities.IntermediaireResponse;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

@Repository
public class IntermediaireRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String urlBase = "";
	
	@Autowired
	private Environment env;


	private Logger logger = LogManager.getLogger(IntermediaireRepository.class);


	public IntermediaireRepository() {
		super();
	}

	public IntermediaireResponse createIntermediaire (Intermediaire intermediaire, String accessToken) {
		logger.log(Level.INFO, "--About to create intermediaire---");
		
		urlBase = env.getProperty("api.dna.intermediaire");


		IntermediaireResponse intermediaireResponse = null;

		//String queryURL = "https://dna-core.sprint-pay.com/api/intermediaire";
		String queryURL = urlBase;
		/*restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken); //"Bearer " +*/

		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPost postRequest = new HttpPost(queryURL);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(intermediaire);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			postRequest.setEntity(entity);



			logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(postRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();

				//Gson gson = new Gson();

				//intermediaireResponse = gson.fromJson(result, IntermediaireResponse.class);

				logger.log(Level.INFO, "-----result-----"+ result);
				
				String description = httpResponse.getStatusLine().toString();

				try {
				intermediaireResponse = objectMapper.readValue(result, IntermediaireResponse.class);

				logger.log(Level.INFO, "----intermediaire response-----" + intermediaireResponse.getCode_intermediaire_dna());
				}catch(UnrecognizedPropertyException upe) {
					
					logger.log(Level.ERROR, "----UnrecognizedPropertyException caught ------");


					ErrorResponse errorResponse = objectMapper.readValue(result, ErrorResponse.class);
					description = errorResponse.getTitle();

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, 4, "TRANS_INT_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----Intermediaire not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "ERROR");
					
					/************On essaie l'update si l'erreur est liée à l'intermédiaire qui existe déjà**********/
					if(errorResponse.getErrorKey().equals("brokerExists")) {
						logger.log(Level.WARN, "--------------Intermédiaire existe déjà, nous allons faire la mise à jour plutôt-----------------");
						return updateIntermediaire(intermediaire, accessToken);
					}

					return null;

				}
				
				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, 3, "TRANS_INT_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----Intermediaire updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, -3, "TRANS_INT_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, 4, "TRANS_INT_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "ERROR");

				}

			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(); 
			}

			/*} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} //new DefaultHttpClient();


		/**ObjectMapper objectMapper = new ObjectMapper();
		String jsonStr;

		jsonStr = objectMapper.writeValueAsString(intermediaire);
		logger.log(Level.INFO, "---jsonStr----" + jsonStr);


		HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
		logger.log(Level.INFO, "---request---"+ request.toString());

		//catch the exception for the case where the access token has expired
		intermediaireResponse = restTemplate.postForObject(queryAuthURL, request, IntermediaireResponse.class);*/

		logger.log(Level.INFO, "*****Intermediaire Response*****" + intermediaireResponse.toString());

		return intermediaireResponse;

	}


	@SuppressWarnings("deprecation")
	public List<Intermediaire> listIntermediaire(int status) {

		String intermediaire = "select * from intermediaire where c_status = ?";

		logger.log(Level.INFO, "----Selecting from intermediaire----");

		List<Intermediaire> listAss = jdbcTemplate.query(intermediaire, new Object[]{status},(rs,rowNum) ->{
			Intermediaire a = new Intermediaire();
			a.setIid(rs.getInt("iid"));
			a.setBoite_postale(StringUtils.trimWhitespace(rs.getString("boite_postale").trim()));
			a.setCode_intermediaire_dna(StringUtils.trimWhitespace(rs.getString("code_intermediaire_dna").trim()));
			a.setNom_intermediaire(StringUtils.trimWhitespace(rs.getString("nom_intermediaire").trim()));
			a.setNum_contribuable(StringUtils.trimWhitespace(rs.getString("num_contribuable").trim()));
			a.setTelephone(StringUtils.trimWhitespace(rs.getString("telephone").trim()));
			a.setType_intermediaire(StringUtils.trimWhitespace(rs.getString("type_intermediaire").trim()));
			a.setVille(StringUtils.trimWhitespace(rs.getString("ville").trim()));

			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------intermediaire size-----" + listAss.size());

		return listAss;

	}

	@SuppressWarnings("deprecation")
	public List<Intermediaire> listIntermediaire(String identifiant_dna, int status) {
		
		logger.log(Level.INFO, "---identifiant_dna---" + identifiant_dna );

		String intermediaire = "select * from intermediaire where identifiant_dna = ? and c_status = ?  ";

		logger.log(Level.INFO, "----Selecting from intermediaire----");

		List<Intermediaire> listAss = jdbcTemplate.query(intermediaire, new Object[]{identifiant_dna, status},(rs,rowNum) ->{
			Intermediaire a = new Intermediaire();
			a.setIid(rs.getInt("iid"));
			a.setBoite_postale(StringUtils.trimWhitespace(rs.getString("boite_postale").trim()));
			a.setCode_intermediaire_dna(StringUtils.trimWhitespace(rs.getString("code_intermediaire_dna").trim()));
			a.setNom_intermediaire(StringUtils.trimWhitespace(rs.getString("nom_intermediaire").trim()));
			a.setNum_contribuable(StringUtils.trimWhitespace(rs.getString("num_contribuable").trim()));
			a.setTelephone(StringUtils.trimWhitespace(rs.getString("telephone").trim()));
			a.setType_intermediaire(StringUtils.trimWhitespace(rs.getString("type_intermediaire").trim()));
			a.setVille(StringUtils.trimWhitespace(rs.getString("ville").trim()));
			
			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------intermediaire size-----" + listAss.size());

		return listAss;

	}
	
	public IntermediaireResponse listIntermediaireFromMINFI(String codeIntermediaire, String accessToken){

		logger.log(Level.INFO, "--About to list intermediaire---");

		urlBase = env.getProperty("api.dna.intermediaire");


		IntermediaireResponse intermediaireResponse = null;

		String queryURL = urlBase+"/"+codeIntermediaire.trim();
		logger.log(Level.INFO, "---queryURL---" + queryURL);

		DefaultHttpClient httpclient;

		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpGet getRequest = new HttpGet(queryURL);
			getRequest.setHeader("Content-type", "application/json");
			getRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + getRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + getRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();


			try {
				HttpResponse httpResponse = httpclient.execute(getRequest);
				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);


				if (entity2 != null && entity2.getContentType()!=null) {

					String result = EntityUtils.toString(entity2);
					entity2.getContent().close();

					logger.log(Level.INFO, "-----result-----"+ result);
					intermediaireResponse = objectMapper.readValue(result, IntermediaireResponse.class);

					logger.log(Level.INFO, "----code intermediaire DNA-----" + intermediaireResponse.getCode_intermediaire_dna());

				}else {
					logger.log(Level.INFO, "-----Entity Content Type Null-----");
				}

			}catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		}catch (Exception e2) {
			e2.printStackTrace();
		}


		return intermediaireResponse;

	}

	public IntermediaireResponse updateIntermediaire (Intermediaire intermediaire, String accessToken) {
		logger.log(Level.INFO, "--About to create intermediaire---");
		
		urlBase = env.getProperty("api.dna.intermediaire");

		IntermediaireResponse intermediaireResponse = null;

		//String queryURL = "https://dna-core.sprint-pay.com/api/intermediaire";
		String queryURL = urlBase;
		/*restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken); //"Bearer " +*/

		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPut putRequest = new HttpPut(queryURL);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + putRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + putRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(intermediaire);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			putRequest.setEntity(entity);



			logger.log(Level.INFO, "-----putRequest.getEntity().toString()-----" + putRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + putRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + putRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(putRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();

				//Gson gson = new Gson();

				//intermediaireResponse = gson.fromJson(result, IntermediaireResponse.class);

				logger.log(Level.INFO, "-----result-----"+ result);
				
				String description = httpResponse.getStatusLine().toString();

				try {
				intermediaireResponse = objectMapper.readValue(result, IntermediaireResponse.class);
				logger.log(Level.INFO, "----UPDATE: intermediaire response-----" + intermediaireResponse.getCode_intermediaire_dna());

				}catch(UnrecognizedPropertyException upe) {
					
					
					logger.log(Level.ERROR, "----UPDATE: UnrecognizedPropertyException caught ------");


					ErrorResponse errorResponse = objectMapper.readValue(result, ErrorResponse.class);
					description = errorResponse.getTitle();

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, 4, "TRANS_INT_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----UPDATE: Intermediaire not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "ERROR");
					
					/************On essaie l'update si l'erreur est liée à l'intermédiaire qui existe déjà**********/
					/**if(errorResponse.getErrorKey().equals("brokerExists")) {
						logger.log(Level.INFO, "--------------Intermédiaire existe déjà, nous allons faire la mise à jour plutôt-----------------");
						return updateIntermediaire(intermediaire, accessToken);
					}*/

					return null;

				}
				
				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, 3, "TRANS_INT_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----UPDATE: Intermediaire updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, -3, "TRANS_INT_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					intermediaire = updateIntermediaireInOurBD(intermediaire, 4, "TRANS_INT_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(intermediaire, description, "ERROR");

				}

				logger.log(Level.INFO, "----UPDATE: intermediaire response-----" + intermediaireResponse.getCode_intermediaire_dna());



			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(); 
			}

			/*} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} //new DefaultHttpClient();


		/**ObjectMapper objectMapper = new ObjectMapper();
		String jsonStr;

		jsonStr = objectMapper.writeValueAsString(intermediaire);
		logger.log(Level.INFO, "---jsonStr----" + jsonStr);


		HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
		logger.log(Level.INFO, "---request---"+ request.toString());

		//catch the exception for the case where the access token has expired
		intermediaireResponse = restTemplate.postForObject(queryAuthURL, request, IntermediaireResponse.class);*/

		logger.log(Level.INFO, "*****Intermediaire Response*****" + intermediaireResponse.toString());

		return intermediaireResponse;

	}


	public Intermediaire updateIntermediaireInOurBD(Intermediaire intermediaire, int status_code, String comment) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date tr_date = dateFormat.parse(strDate);

			String update_assure = "update intermediaire set c_status = ?, c_date_transfer = ?, commentaires = ? where code_intermediaire_dna = ? and num_contribuable = ? ";

			Object[] params = new Object[] {status_code, tr_date, comment, intermediaire.getCode_intermediaire_dna(), intermediaire.getNum_contribuable()};

			int update =  jdbcTemplate.update(update_assure, params);

			logger.log(Level.INFO, "---update---" + update);

			if(update!=1) {
				return null;
			}
			
			intermediaire = listIntermediaire(intermediaire.getCode_intermediaire_dna(), status_code).get(0);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return intermediaire;

	}

	public Boolean insertIntoJournal(Intermediaire intermediaire, String description, String status) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


		try {

			Date dateInscription = dateFormat.parse(strDate);

			//String insertIntoJournal = "Insert into journal (reference, description, status, date_inscription) values (?, ?, ?, ?)";

			String insertIntoJournal = "INSERT INTO journal_intermediaire (code_intermediaire_dna, num_contribuable, type_intermediaire, nom_intermediaire, telephone, ville, boite_postale,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires, j_logdate) "+
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

			
			//String updateJournal = "Update journal set description = ?, status = ?, date_inscription = ? where reference = ?";


			try {
				
				//Object[] params = new Object[] {"TRANSINT"+String.valueOf(intermediaire.getIid()), description, status, dateInscription};

				Object[] params = new Object[] {intermediaire.getCode_intermediaire_dna(), intermediaire.getNum_contribuable(), intermediaire.getType_intermediaire(), intermediaire.getNom_intermediaire(), intermediaire.getTelephone(), intermediaire.getVille(), intermediaire.getBoite_postale(), 
						intermediaire.getC_status(), intermediaire.getC_date_creation(), intermediaire.getC_date_mis_a_jour(), intermediaire.getC_date_transfer(), intermediaire.getCommentaires(), dateInscription};

				int insert =  jdbcTemplate.update(insertIntoJournal, params);

				logger.log(Level.INFO, "---insert---" + insert);

				if(insert!=1) {
					return Boolean.FALSE;
				}

			}catch(DuplicateKeyException dke) {
				
				/**Object[] params = new Object[] {description, status, dateInscription,String.valueOf(assure.getAid())};

				int update =  jdbcTemplate.update(updateJournal, params);

				logger.log(Level.INFO, "---update---" + update);

				if(update!=1) {
					return Boolean.FALSE;
				}*/
				dke.printStackTrace();
				
			}

		}catch (ParseException e) {
			e.printStackTrace();
		}

		return Boolean.TRUE;

	}
}
