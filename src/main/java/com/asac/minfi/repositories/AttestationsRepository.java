package com.asac.minfi.repositories;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.asac.minfi.entities.Attestation;
import com.asac.minfi.entities.AttestationResponse;
import com.asac.minfi.entities.ErrorResponse;
import com.asac.minfi.entities.ErrorResponse2;
import com.asac.minfi.entities.ErrorResponse3;
import com.asac.minfi.entities.RemorquesResponse;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

@Repository
public class AttestationsRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String urlBase = "";

	private Logger logger = LogManager.getLogger(AttestationsRepository.class);


	@Autowired
	private Environment env;

	public AttestationsRepository() {
		super();
	}

	public AttestationResponse createAttestation (Attestation attestation, String accessToken) {
		logger.log(Level.INFO, "--About to create attestation---");

		urlBase = env.getProperty("api.dna.attestation");


		AttestationResponse attestationResponse = null;

		//String queryURL = "https://dna-core.sprint-pay.com/api/attestations";
		String queryURL = urlBase;

		/*restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken); //"Bearer " +*/

		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPost postRequest = new HttpPost(queryURL);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(attestation);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			postRequest.setEntity(entity);



			logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(postRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();

				//Gson gson = new Gson();

				//attestationResponse = gson.fromJson(result, AttestationResponse.class);

				logger.log(Level.INFO, "-----result-----"+ result);
				attestationResponse = objectMapper.readValue(result, AttestationResponse.class);

				logger.log(Level.INFO, "----attestation response-----" + attestationResponse.getNumero_attestation());


			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(); 
			}

			/*} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} //new DefaultHttpClient();


		/**ObjectMapper objectMapper = new ObjectMapper();
		String jsonStr;

		jsonStr = objectMapper.writeValueAsString(attestation);
		logger.log(Level.INFO, "---jsonStr----" + jsonStr);


		HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
		logger.log(Level.INFO, "---request---"+ request.toString());

		//catch the exception for the case where the access token has expired
		attestationResponse = restTemplate.postForObject(queryAuthURL, request, AttestationResponse.class);*/

		logger.log(Level.INFO, "*****Attestation Response*****" + attestationResponse.toString());

		return attestationResponse;

	}


	@SuppressWarnings("deprecation")
	public List<Attestation> listAttestation(int status) {

		String attestation = "select * from attestation where c_status = ?";

		logger.log(Level.INFO, "----Selecting from attestation----");

		List<Attestation> listAss = jdbcTemplate.query(attestation, new Object[]{status},(rs,rowNum) ->{
			Attestation a = new Attestation();
			a.setAid(rs.getInt("aid"));
			a.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			a.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			a.setCouleur(StringUtils.trimWhitespace(rs.getString("couleur").trim()));
			a.setDate_echeance(StringUtils.trimWhitespace(rs.getString("date_echeance").trim()));
			a.setDate_effet(StringUtils.trimWhitespace(rs.getString("date_effet").trim()));
			a.setDate_emission(StringUtils.trimWhitespace(rs.getString("date_emission").trim()));
			a.setImmatriculation(StringUtils.trimWhitespace(rs.getString("immatriculation").trim()));
			a.setNumero_attestation(StringUtils.trimWhitespace(rs.getString("numero_attestation").trim()));
			a.setNumero_police(StringUtils.trimWhitespace(rs.getString("numero_police").trim()));
			a.setStatut(StringUtils.trimWhitespace(rs.getString("statut").trim()));
			a.setZone_circulation(StringUtils.trimWhitespace(rs.getString("zone_circulation").trim()));
			a.setRemorque(StringUtils.trimWhitespace(rs.getString("remorque").trim()));


			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------attestation size-----" + listAss.size());

		return listAss;

	}

	@SuppressWarnings("deprecation")
	public List<Attestation> listAttestation(String numero_attestation, int status) {

		logger.log(Level.INFO, "---numero_attestation---" + numero_attestation );

		String attestation = "select * from attestation where numero_attestation = ? and c_status = ?";

		logger.log(Level.INFO, "----Selecting from attestation----");

		List<Attestation> listAss = jdbcTemplate.query(attestation, new Object[]{numero_attestation,status},(rs,rowNum) ->{
			Attestation a = new Attestation();
			a.setAid(rs.getInt("aid"));
			a.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			a.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			a.setCouleur(StringUtils.trimWhitespace(rs.getString("couleur").trim()));
			a.setDate_echeance(StringUtils.trimWhitespace(rs.getString("date_echeance").trim()));
			a.setDate_effet(StringUtils.trimWhitespace(rs.getString("date_effet").trim()));
			a.setDate_emission(StringUtils.trimWhitespace(rs.getString("date_emission").trim()));
			a.setImmatriculation(StringUtils.trimWhitespace(rs.getString("immatriculation").trim()));
			a.setNumero_attestation(StringUtils.trimWhitespace(rs.getString("numero_attestation").trim()));
			a.setNumero_police(StringUtils.trimWhitespace(rs.getString("numero_police").trim()));
			a.setStatut(StringUtils.trimWhitespace(rs.getString("statut").trim()));
			a.setZone_circulation(StringUtils.trimWhitespace(rs.getString("zone_circulation").trim()));
			a.setRemorque(StringUtils.trimWhitespace(rs.getString("remorque").trim()));

			a.setC_status(rs.getInt("c_status"));
			a.setC_date_creation(rs.getTimestamp("c_date_creation"));
			a.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			a.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			a.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return a;});

		logger.log(Level.INFO, "------attestation size-----" + listAss.size());

		return listAss;

	}

	public AttestationResponse listAttestationsFromMINFI(String numAttestation, String accessToken){

		logger.log(Level.INFO, "--About to list attestations---");

		urlBase = env.getProperty("api.dna.attestation");

		AttestationResponse attestationResponse = null;

		String queryURL = urlBase+"/"+numAttestation.trim();
		logger.log(Level.INFO, "---queryURL---" + queryURL);

		DefaultHttpClient httpclient;

		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpGet getRequest = new HttpGet(queryURL);
			getRequest.setHeader("Content-type", "application/json");
			getRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + getRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + getRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();


			try {
				HttpResponse httpResponse = httpclient.execute(getRequest);
				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);


				if (entity2 != null && entity2.getContentType()!=null) {
					// return it as a String
					String result = EntityUtils.toString(entity2);
					entity2.getContent().close();

					logger.log(Level.INFO, "-----result-----"+ result);
					attestationResponse = objectMapper.readValue(result, AttestationResponse.class);

					logger.log(Level.INFO, "----attestation response numAtt -----" + attestationResponse.getNumero_attestation());

				}

			}catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}


		}catch (Exception e2) {
			e2.printStackTrace();
		}


		return attestationResponse;

	}


	public AttestationResponse updateAttestation (Attestation attestation, String accessToken) {
		logger.log(Level.INFO, "--About to update attestation---");

		urlBase = env.getProperty("api.dna.attestation");


		AttestationResponse attestationResponse = null;

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPut putRequest = new HttpPut(queryURL);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + putRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + putRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(attestation);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			putRequest.setEntity(entity);



			logger.log(Level.INFO, "-----putRequest.getEntity().toString()-----" + putRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + putRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + putRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(putRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();

				//Gson gson = new Gson();

				//attestationResponse = gson.fromJson(result, AttestationResponse.class);

				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {
					attestationResponse = objectMapper.readValue(result, AttestationResponse.class);

					logger.log(Level.INFO, "----attestation response-----" + attestationResponse.getNumero_attestation());

				}catch(UnrecognizedPropertyException upe) {

					
					ErrorResponse errorResponse;
					ErrorResponse3 errorResponse3;

					logger.log(Level.ERROR, "----Unrecognized property exception caught ------");

					try {

						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();

					}catch(UnrecognizedPropertyException upe2) {

						errorResponse3 = objectMapper.readValue(result, ErrorResponse3.class);
						//description = errorResponse3.getViolations().get(0).getField() + " : " + errorResponse3.getViolations().get(0).getMessage();
						description = errorResponse3.getTitle();
					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					attestation = updateAttestationsInOurBD(attestation, 4, "TRANS_ATT_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----Attestation not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(attestation, description, "ERROR");

					////////Voir avec l'équipe si c'est possible de créer l'attestation ici, lorsqu'on reçoit un save transient instance après l'appel de l'update///////////////

					return null;

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					attestation = updateAttestationsInOurBD(attestation, 3, "TRANS_ATT_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----Attestation updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(attestation, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					attestation = updateAttestationsInOurBD(attestation, -3, "TRANS_ATT_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(attestation, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					attestation = updateAttestationsInOurBD(attestation, 4, "TRANS_ATT_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(attestation, description, "ERROR");

				}


			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(); 
			}

			/*} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} //new DefaultHttpClient();


		/**ObjectMapper objectMapper = new ObjectMapper();
		String jsonStr;

		jsonStr = objectMapper.writeValueAsString(attestation);
		logger.log(Level.INFO, "---jsonStr----" + jsonStr);


		HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
		logger.log(Level.INFO, "---request---"+ request.toString());

		//catch the exception for the case where the access token has expired
		attestationResponse = restTemplate.postForObject(queryAuthURL, request, AttestationResponse.class);*/
		if(attestationResponse!=null)
			logger.log(Level.INFO, "*****Attestation Response*****" + attestationResponse.toString());

		return attestationResponse;

	}


	public Attestation updateAttestationsInOurBD(Attestation attestation, int status_code, String comment) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date tr_date = dateFormat.parse(strDate);

			String update_assure = "update attestation set c_status = ?, c_date_transfer = ?, commentaires = ? where numero_attestation = ? and immatriculation = ? ";

			Object[] params = new Object[] {status_code, tr_date, comment, attestation.getNumero_attestation(), attestation.getImmatriculation()};

			int update =  jdbcTemplate.update(update_assure, params);

			logger.log(Level.INFO, "---update---" + update);

			if(update!=1) {
				return null;
			}

			attestation = listAttestation(attestation.getNumero_attestation(), status_code).get(0);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return attestation;

	}

	public Boolean insertIntoJournal(Attestation attestation, String description, String status) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


		try {

			Date dateInscription = dateFormat.parse(strDate);

			//String insertIntoJournal = "Insert into journal (reference, description, status, date_inscription) values (?, ?, ?, ?)";

			String insertIntoJournal = "INSERT INTO journal_attestation (numero_attestation, numero_police, date_emission, date_effet, date_echeance, couleur, statut, zone_circulation, immatriculation, code_assure, code_assureur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, remorque, commentaires, j_logdate) " +
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?);";


			//String updateJournal = "Update journal set description = ?, status = ?, date_inscription = ? where reference = ?";



			try {

				//Object[] params = new Object[] {"TRANSATT"+String.valueOf(attestation.getAid()), description, status, dateInscription};
				Object[] params = new Object[] {attestation.getNumero_attestation(), attestation.getNumero_police(), sdf.parse(attestation.getDate_emission()), sdf.parse(attestation.getDate_effet()), sdf.parse(attestation.getDate_echeance()),attestation.getCouleur(), attestation.getStatut(), attestation.getZone_circulation(), attestation.getImmatriculation(),
						attestation.getCode_assure(), attestation.getCode_assureur(),attestation.getC_status(), attestation.getC_date_creation(), attestation.getC_date_mis_a_jour(), attestation.getC_date_transfer(), attestation.getRemorque(), attestation.getCommentaires(), dateInscription};

				int insert =  jdbcTemplate.update(insertIntoJournal, params);

				logger.log(Level.INFO, "---insert---" + insert);

				if(insert!=1) {
					return Boolean.FALSE;
				}

			}catch(DuplicateKeyException dke) {

				/**Object[] params = new Object[] {description, status, dateInscription,String.valueOf(assure.getAid())};

				int update =  jdbcTemplate.update(updateJournal, params);

				logger.log(Level.INFO, "---update---" + update);

				if(update!=1) {
					return Boolean.FALSE;
				}*/
				dke.printStackTrace();

			}

		}catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Boolean.TRUE;

	}

}
