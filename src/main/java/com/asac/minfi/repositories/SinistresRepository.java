package com.asac.minfi.repositories;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpClientConnectionOperator;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.ErrorResponse;
import com.asac.minfi.entities.RestTemplateWithoutSSL;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.entities.SinistresResponse;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.google.gson.Gson;

@Repository
public class SinistresRepository {

	@Autowired()
	private JdbcTemplate jdbcTemplate;

	/*@Autowired(required = false)
	private RestTemplate restTemplate;*/

	private String urlBase = "";

	@Autowired
	private Environment env;

	private Logger logger = LogManager.getLogger(SinistresRepository.class);



	public SinistresResponse createSinistre(Sinistres sinistres, String accessToken) {

		logger.log(Level.INFO, "--About to create sinistres---");

		urlBase = env.getProperty("api.dna.sinistre");

		SinistresResponse sinistresResponse = null;

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPost postRequest = new HttpPost(queryURL);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(sinistres);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			postRequest.setEntity(entity);



			logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(postRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {
					sinistresResponse = objectMapper.readValue(result, SinistresResponse.class);

					logger.log(Level.INFO, "----sinistre response---" + sinistresResponse.getNumero_attestation());

				}catch(UnrecognizedPropertyException upe) {

					logger.log(Level.ERROR, "----UnrecognizedPropertyException caught ------");

					ErrorResponse errorResponse = objectMapper.readValue(result, ErrorResponse.class);
					description = errorResponse.getTitle();

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, 4, "TRANS_SIN_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----Sinistre not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "ERROR");


					return null;

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, 3, "TRANS_SIN_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----Sinistre updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, -3, "TRANS_SIN_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, 4, "TRANS_SIN_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "ERROR");

				}


			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		} 

		if(sinistresResponse!=null)
			logger.log(Level.INFO, "*****Sinistres Response*****" + sinistresResponse.toString());

		return sinistresResponse;
	}


	@SuppressWarnings("deprecation")
	public List<Sinistres> listSinistres(int status) {
		// TODO Auto-generated method stub

		String sinistres = "select * from sinistre where c_status = ?";

		logger.log(Level.INFO, "----Selecting from sinistre----");

		List<Sinistres> listSins = jdbcTemplate.query(sinistres, new Object[]{status},(rs,rowNum) ->{
			Sinistres s = new Sinistres();
			s.setSid(rs.getInt("sid"));
			s.setCause_sinistre(StringUtils.trimWhitespace(rs.getString("cause_sinistre").trim()));
			s.setDate_declaration(StringUtils.trimWhitespace(rs.getString("date_declaration").trim()));
			s.setDate_survenance(StringUtils.trimWhitespace(rs.getString("date_survenance").trim()));
			s.setNumero_attestation(StringUtils.trimWhitespace(rs.getString("numero_attestation").trim()));
			s.setReference(StringUtils.trimWhitespace(rs.getString("reference").trim()));
			s.setType_dommage(StringUtils.trimWhitespace(rs.getString("type_dommage").trim()));
			s.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));


			s.setC_status(rs.getInt("c_status"));
			s.setC_date_creation(rs.getTimestamp("c_date_creation"));
			s.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			s.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			s.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return s;});

		logger.log(Level.INFO, "------Sinistres size-----" + listSins.size());


		return listSins;

	}

	@SuppressWarnings("deprecation")
	public List<Sinistres> listSinistres(String numero_attestation, String reference, int status) {
		// TODO Auto-generated method stub

		String sinistres = "select * from sinistre where numero_attestation = ? and reference = ? and c_status = ?";

		logger.log(Level.INFO, "----Selecting from sinistre----");

		List<Sinistres> listSins = jdbcTemplate.query(sinistres, new Object[]{numero_attestation, reference, status},(rs,rowNum) ->{
			Sinistres s = new Sinistres();
			s.setSid(rs.getInt("sid"));
			s.setCause_sinistre(StringUtils.trimWhitespace(rs.getString("cause_sinistre").trim()));
			s.setDate_declaration(StringUtils.trimWhitespace(rs.getString("date_declaration").trim()));
			s.setDate_survenance(StringUtils.trimWhitespace(rs.getString("date_survenance").trim()));
			s.setNumero_attestation(StringUtils.trimWhitespace(rs.getString("numero_attestation").trim()));
			s.setReference(StringUtils.trimWhitespace(rs.getString("reference").trim()));
			s.setType_dommage(StringUtils.trimWhitespace(rs.getString("type_dommage").trim()));
			s.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));


			s.setC_status(rs.getInt("c_status"));
			s.setC_date_creation(rs.getTimestamp("c_date_creation"));
			s.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			s.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			s.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));


			return s;});

		logger.log(Level.INFO, "------Sinistres size-----" + listSins.size());


		return listSins;

	}

	public SinistresResponse listSinistresFromMINFI(String numero_attestation, String reference, String accessToken){

		logger.log(Level.INFO, "--About to list assure---");

		urlBase = env.getProperty("api.dna.sinistre");

		SinistresResponse sinistresResponse = null;

		String queryURL = urlBase + "/" + reference + "/" + numero_attestation;
		logger.log(Level.INFO, "---queryURL---" + queryURL);

		DefaultHttpClient httpclient;

		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpGet getRequest = new HttpGet(queryURL);
			getRequest.setHeader("Content-type", "application/json");
			getRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + getRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + getRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();


			try {
				HttpResponse httpResponse = httpclient.execute(getRequest);
				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);


				if (entity2 != null && entity2.getContentType()!=null) {
					// return it as a String
					String result = EntityUtils.toString(entity2);
					entity2.getContent().close();

					logger.log(Level.INFO, "-----result-----"+ result);
					sinistresResponse = objectMapper.readValue(result, SinistresResponse.class);

					logger.log(Level.INFO, "----sinistres response N° Attestation-----" + sinistresResponse.getNumero_attestation());

				}else {
					logger.log(Level.INFO, "-----Entity Content Type Null-----");
				}

			}catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		}catch (Exception e2) {
			e2.printStackTrace();
		}


		return sinistresResponse;

	}


	public SinistresResponse updateSinistre(Sinistres sinistres, String accessToken) {

		logger.log(Level.INFO, "--About to update sinistres---");

		urlBase = env.getProperty("api.dna.sinistre");


		SinistresResponse sinistresResponse = null;

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPut putRequest = new HttpPut(queryURL);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + putRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + putRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(sinistres);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			putRequest.setEntity(entity);



			logger.log(Level.INFO, "-----putRequest.getEntity().toString()-----" + putRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + putRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + putRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(putRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {
					sinistresResponse = objectMapper.readValue(result, SinistresResponse.class);
					logger.log(Level.INFO, "---UPDATE: sinistre response---" + sinistresResponse.getNumero_attestation());

				}catch(UnrecognizedPropertyException upe) {

					logger.log(Level.ERROR, "---UPDATE: UnrecognizedPropertyException caught ----");


					ErrorResponse errorResponse = objectMapper.readValue(result, ErrorResponse.class);
					description = errorResponse.getTitle();

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, 4, "TRANS_SIN_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----UPDATE: Sinistre not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "ERROR");

					return null;

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, 3, "TRANS_SIN_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----UPDATE: Sinistre updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, -3, "TRANS_SIN_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					sinistres = updateSinistresInOurBD(sinistres, 4, "TRANS_SIN_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(sinistres, description, "ERROR");

				}

				logger.log(Level.INFO, "----UPDATE: sinistre response---" + sinistresResponse.getNumero_attestation());


			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}


		} catch (Exception e2) {
			e2.printStackTrace();
		}


		if(sinistresResponse!=null)
			logger.log(Level.INFO, "*****Sinistres Response*****" + sinistresResponse.toString());

		return sinistresResponse;
	}

	public Sinistres updateSinistresInOurBD(Sinistres sinistres, int status_code, String comment) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date tr_date = dateFormat.parse(strDate);

			String update_assure = "update sinistre set c_status = ?, c_date_transfer = ?, commentaires = ? where numero_attestation = ? and reference = ? ";

			Object[] params = new Object[] {status_code, tr_date, comment, sinistres.getNumero_attestation(), sinistres.getReference()};

			int update =  jdbcTemplate.update(update_assure, params);

			logger.log(Level.INFO, "---update---" + update);

			if(update!=1) {
				return null;
			}

			sinistres = listSinistres(sinistres.getNumero_attestation(), sinistres.getReference(), status_code).get(0);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sinistres;

	}

	public Boolean insertIntoJournal(Sinistres sinistres, String description, String status) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {

			Date dateInscription = dateFormat.parse(strDate);

			//String insertIntoJournal = "Insert into journal (reference, description, status, date_inscription) values (?, ?, ?, ?)";

			String insertIntoJournal = "INSERT INTO journal_sinistre (code_assureur, reference, type_dommage, cause_sinistre, numero_attestation, date_survenance, date_declaration, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires, j_logdate) " +
					"VALUES (?,?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?);";


			//String updateJournal = "Update journal set description = ?, status = ?, date_inscription = ? where reference = ?";

			try {

				//Object[] params = new Object[] {"TRANSSIN"+String.valueOf(sinistres.getSid()), description, status, dateInscription};

				Object[] params = new Object[] {sinistres.getCode_assureur(), sinistres.getReference(), sinistres.getType_dommage(), sinistres.getCause_sinistre(), sinistres.getNumero_attestation(), sdf.parse(sinistres.getDate_survenance()), sdf.parse(sinistres.getDate_declaration()),
						sinistres.getC_status(), sinistres.getC_date_creation(), sinistres.getC_date_mis_a_jour(), sinistres.getC_date_transfer(), sinistres.getCommentaires(), dateInscription};

				int insert =  jdbcTemplate.update(insertIntoJournal, params);

				logger.log(Level.INFO, "---insert---" + insert);

				if(insert!=1) {
					return Boolean.FALSE;
				}

			}catch(DuplicateKeyException dke) {

				/**Object[] params = new Object[] {description, status, dateInscription,String.valueOf(assure.getAid())};

				int update =  jdbcTemplate.update(updateJournal, params);

				logger.log(Level.INFO, "---update---" + update);

				if(update!=1) {
					return Boolean.FALSE;
				}*/
				dke.printStackTrace();

			}

		}catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Boolean.TRUE;

	}

}
