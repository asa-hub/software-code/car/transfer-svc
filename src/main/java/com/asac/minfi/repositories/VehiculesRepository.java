package com.asac.minfi.repositories;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpClientConnectionOperator;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.AdressesResponse;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.AuthentificationResponse;
import com.asac.minfi.entities.ErrorResponse;
import com.asac.minfi.entities.ErrorResponse2;
import com.asac.minfi.entities.RestTemplateWithoutSSL;
import com.asac.minfi.entities.Vehicules;
import com.asac.minfi.entities.VehiculesResponse;
import com.asac.minfi.utils.ASACMinfiUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.google.gson.Gson;

@Repository
public class VehiculesRepository {

	@Autowired()
	private JdbcTemplate jdbcTemplate;

	/*@Autowired(required = false)
	private RestTemplate restTemplate;*/

	private String urlBase = "";

	@Autowired
	private Environment env;

	private Logger logger = LogManager.getLogger(VehiculesRepository.class);


	public VehiculesResponse createVehicule(Vehicules vehicules, String accessToken) {

		logger.log(Level.INFO, "--About to create vehicules---");

		urlBase = env.getProperty("api.dna.vehicule");

		VehiculesResponse vehiculesResponse = null;

		String queryURL = urlBase;


		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPost postRequest = new HttpPost(queryURL);
			postRequest.setHeader("Content-type", "application/json");
			postRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(vehicules);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			postRequest.setEntity(entity);


			logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(postRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();

				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {

					vehiculesResponse = objectMapper.readValue(result, VehiculesResponse.class);

					logger.log(Level.INFO, "----vehicule response Immatriculation---" + vehiculesResponse.getImmatriculation());

				}catch(UnrecognizedPropertyException upe) {

					ErrorResponse errorResponse;
					ErrorResponse2 errorResponse2;

					logger.log(Level.ERROR, "----UnrecognizedPropertyException caught ---");

					try {
						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();
					}catch(UnrecognizedPropertyException upe2) {
						errorResponse2 = objectMapper.readValue(result, ErrorResponse2.class);
						description = errorResponse2.getViolations().get(0).getField() + " : " + errorResponse2.getViolations().get(0).getMessage();

					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, 4, "TRANS_VEH_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----Vehicule not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "ERROR");

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, 3, "TRANS_VEH_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----Vehicule updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, -3, "TRANS_VEH_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, 4, "TRANS_VEH_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "ERROR");

				}


			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		} 


		if(vehiculesResponse!=null)
			logger.log(Level.INFO, "*****Vehicules Response*****" + vehiculesResponse.toString());

		return vehiculesResponse;
	}


	@SuppressWarnings("deprecation")
	public List<Vehicules> listVehicules(int status) {
		// TODO Auto-generated method stub

		String vehicules = "select * from vehicule where c_status = ?";

		logger.log(Level.INFO, "----Selecting from vehicule----");

		List<Vehicules> listSins = jdbcTemplate.query(vehicules, new Object[]{status},(rs,rowNum) ->{

			Vehicules s = new Vehicules();

			s.setVid(rs.getInt("vid"));
			s.setCharge_utile(rs.getString("charge_utile")!=null?StringUtils.trimWhitespace(rs.getString("charge_utile").trim()):null);
			s.setChassis(rs.getString("chassis")!=null?StringUtils.trimWhitespace(rs.getString("chassis").trim()):null);
			s.setCode_assure(rs.getString("code_assure")!=null?StringUtils.trimWhitespace(rs.getString("code_assure").trim()):null);
			s.setCode_assureur(rs.getString("code_assureur")!=null?StringUtils.trimWhitespace(rs.getString("code_assureur").trim()):null);
			s.setDate_premiere_mise_circulation(rs.getString("date_premiere_mise_circulation")!=null?StringUtils.trimWhitespace(rs.getString("date_premiere_mise_circulation").trim()):null);
			s.setDouble_commande(rs.getString("double_commande")!=null?StringUtils.trimWhitespace(rs.getString("double_commande").trim()):"0");
			s.setImmatriculation(rs.getString("immatriculation")!=null?StringUtils.trimWhitespace(rs.getString("immatriculation").trim()):null);
			s.setImmatriculation_remorque(rs.getString("immatriculation_remorque")!=null?StringUtils.trimWhitespace(rs.getString("immatriculation_remorque").trim()):"");
			s.setMarque(rs.getString("marque")!=null?StringUtils.trimWhitespace(rs.getString("marque").trim()):null);
			s.setModele(rs.getString("modele")!=null?StringUtils.trimWhitespace(rs.getString("modele").trim()):null);
			s.setCylindree(rs.getString("cylindree")!=null?StringUtils.trimWhitespace(rs.getString("cylindree")):null);
			s.setNombre_de_places(rs.getString("nombre_de_places")!=null?StringUtils.trimWhitespace(rs.getString("nombre_de_places").trim()):null);
			s.setNombre_portes(rs.getString("nombre_portes")!=null?StringUtils.trimWhitespace(rs.getString("nombre_portes")):null);
			//s.setPoids(StringUtils.trimWhitespace(rs.getString("poids").trim()));
			s.setPoids_total_autorise_en_charge(rs.getString("poids_total_autorise_en_charge")!=null?StringUtils.trimWhitespace(rs.getString("poids_total_autorise_en_charge").trim()):null);
			s.setPuissance_fiscale(rs.getString("puissance_fiscale")!=null?StringUtils.trimWhitespace(rs.getString("puissance_fiscale").trim()):null);
			s.setResponsabilite_civile(rs.getString("responsabilite_civile")!=null?StringUtils.trimWhitespace(rs.getString("responsabilite_civile").trim()):null);
			s.setRemorque(rs.getString("remorque")!=null?StringUtils.trimWhitespace(rs.getString("remorque").trim()):null);
			s.setSource_energie(rs.getString("source_energie")!=null?StringUtils.trimWhitespace(rs.getString("source_energie").trim()):null);
			s.setType_engin(rs.getString("type_engin")!=null?StringUtils.trimWhitespace(rs.getString("type_engin")):null);
			s.setUsage(rs.getString("usage")!=null?StringUtils.trimWhitespace(rs.getString("usage").trim()):null);
			s.setUtilitaire(rs.getString("utilitaire")!=null?StringUtils.trimWhitespace(rs.getString("utilitaire")):null);


			s.setC_status(rs.getInt("c_status"));
			s.setC_date_creation(rs.getTimestamp("c_date_creation"));
			s.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			s.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			s.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return s;});

		logger.log(Level.INFO, "------Vehicules size-----" + listSins.size());


		return listSins;

	}

	@SuppressWarnings("deprecation")
	public List<Vehicules> listVehicules(String immatriculation, int status) {
		// TODO Auto-generated method stub

		String vehicules = "select * from vehicule where immatriculation = ? and c_status = ?";

		logger.log(Level.INFO, "----Selecting from vehicule----");


		List<Vehicules> listSins = jdbcTemplate.query(vehicules, new Object[]{immatriculation,status},(rs,rowNum) ->{
			Vehicules s = new Vehicules();

			/*s.setVid(rs.getInt("vid"));
			s.setCharge_utile(StringUtils.trimWhitespace(rs.getString("charge_utile").trim()));
			s.setChassis(StringUtils.trimWhitespace(rs.getString("chassis").trim()));
			s.setCode_assure(StringUtils.trimWhitespace(rs.getString("code_assure").trim()));
			s.setCode_assureur(StringUtils.trimWhitespace(rs.getString("code_assureur").trim()));
			s.setDate_premiere_mise_circulation(StringUtils.trimWhitespace(rs.getString("date_premiere_mise_circulation").trim()));
			s.setDouble_commande(rs.getString("double_commande")!=null?StringUtils.trimWhitespace(rs.getString("double_commande").trim()):"0");
			s.setImmatriculation(StringUtils.trimWhitespace(rs.getString("immatriculation").trim()));
			s.setImmatriculation_remorque(StringUtils.trimWhitespace(rs.getString("immatriculation_remorque").trim()));
			s.setMarque(StringUtils.trimWhitespace(rs.getString("marque").trim()));
			s.setModele(StringUtils.trimWhitespace(rs.getString("modele").trim()));
			s.setCylindree(StringUtils.trimWhitespace(rs.getString("cylindree")));
			s.setNombre_de_places(StringUtils.trimWhitespace(rs.getString("nombre_de_places").trim()));
			s.setNombre_portes(StringUtils.trimWhitespace(rs.getString("nombre_portes")));
			//s.setPoids(StringUtils.trimWhitespace(rs.getString("poids").trim()));
			s.setPoids_total_autorise_en_charge(StringUtils.trimWhitespace(rs.getString("poids_total_autorise_en_charge").trim()));
			s.setPuissance_fiscale(StringUtils.trimWhitespace(rs.getString("puissance_fiscale").trim()));
			s.setResponsabilite_civile(StringUtils.trimWhitespace(rs.getString("responsabilite_civile").trim()));
			s.setRemorque(StringUtils.trimWhitespace(rs.getString("remorque").trim()));
			s.setSource_energie(StringUtils.trimWhitespace(rs.getString("source_energie").trim()));
			s.setType_engin(StringUtils.trimWhitespace(rs.getString("type_engin")));
			s.setUsage(StringUtils.trimWhitespace(rs.getString("usage").trim()));
			s.setUtilitaire(StringUtils.trimWhitespace(rs.getString("utilitaire")));*/
			
			s.setVid(rs.getInt("vid"));
			s.setCharge_utile(rs.getString("charge_utile")!=null?StringUtils.trimWhitespace(rs.getString("charge_utile").trim()):null);
			s.setChassis(rs.getString("chassis")!=null?StringUtils.trimWhitespace(rs.getString("chassis").trim()):null);
			s.setCode_assure(rs.getString("code_assure")!=null?StringUtils.trimWhitespace(rs.getString("code_assure").trim()):null);
			s.setCode_assureur(rs.getString("code_assureur")!=null?StringUtils.trimWhitespace(rs.getString("code_assureur").trim()):null);
			s.setDate_premiere_mise_circulation(rs.getString("date_premiere_mise_circulation")!=null?StringUtils.trimWhitespace(rs.getString("date_premiere_mise_circulation").trim()):null);
			s.setDouble_commande(rs.getString("double_commande")!=null?StringUtils.trimWhitespace(rs.getString("double_commande").trim()):"0");
			s.setImmatriculation(rs.getString("immatriculation")!=null?StringUtils.trimWhitespace(rs.getString("immatriculation").trim()):null);
			s.setImmatriculation_remorque(rs.getString("immatriculation_remorque")!=null?StringUtils.trimWhitespace(rs.getString("immatriculation_remorque").trim()):"");
			s.setMarque(rs.getString("marque")!=null?StringUtils.trimWhitespace(rs.getString("marque").trim()):null);
			s.setModele(rs.getString("modele")!=null?StringUtils.trimWhitespace(rs.getString("modele").trim()):null);
			s.setCylindree(rs.getString("cylindree")!=null?StringUtils.trimWhitespace(rs.getString("cylindree")):null);
			s.setNombre_de_places(rs.getString("nombre_de_places")!=null?StringUtils.trimWhitespace(rs.getString("nombre_de_places").trim()):null);
			s.setNombre_portes(rs.getString("nombre_portes")!=null?StringUtils.trimWhitespace(rs.getString("nombre_portes")):null);
			//s.setPoids(StringUtils.trimWhitespace(rs.getString("poids").trim()));
			s.setPoids_total_autorise_en_charge(rs.getString("poids_total_autorise_en_charge")!=null?StringUtils.trimWhitespace(rs.getString("poids_total_autorise_en_charge").trim()):null);
			s.setPuissance_fiscale(rs.getString("puissance_fiscale")!=null?StringUtils.trimWhitespace(rs.getString("puissance_fiscale").trim()):null);
			s.setResponsabilite_civile(rs.getString("responsabilite_civile")!=null?StringUtils.trimWhitespace(rs.getString("responsabilite_civile").trim()):null);
			s.setRemorque(rs.getString("remorque")!=null?StringUtils.trimWhitespace(rs.getString("remorque").trim()):null);
			s.setSource_energie(rs.getString("source_energie")!=null?StringUtils.trimWhitespace(rs.getString("source_energie").trim()):null);
			s.setType_engin(rs.getString("type_engin")!=null?StringUtils.trimWhitespace(rs.getString("type_engin")):null);
			s.setUsage(rs.getString("usage")!=null?StringUtils.trimWhitespace(rs.getString("usage").trim()):null);
			s.setUtilitaire(rs.getString("utilitaire")!=null?StringUtils.trimWhitespace(rs.getString("utilitaire")):null);


			s.setC_status(rs.getInt("c_status"));
			s.setC_date_creation(rs.getTimestamp("c_date_creation"));
			s.setC_date_mis_a_jour(rs.getTimestamp("c_date_mis_a_jour"));
			s.setC_date_transfer(rs.getTimestamp("c_date_transfer"));
			s.setCommentaires(StringUtils.trimWhitespace(rs.getString("commentaires")));

			return s;});

		logger.log(Level.INFO, "------Vehicules size-----" + listSins.size());


		return listSins;

	}

	public VehiculesResponse listVehiculeFromMINFI(String immatriculation, String accessToken){

		logger.log(Level.INFO, "--About to list vehicule---");

		urlBase = env.getProperty("api.dna.vehicule");

		VehiculesResponse vehiculesResponse = null;

		String queryURL = urlBase + "/" + immatriculation;
		logger.log(Level.INFO, "---queryURL---" + queryURL);

		DefaultHttpClient httpclient;

		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpGet getRequest = new HttpGet(queryURL);
			getRequest.setHeader("Content-type", "application/json");
			getRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + getRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + getRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();


			try {
				HttpResponse httpResponse = httpclient.execute(getRequest);
				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);



				if (entity2 != null  && entity2.getContentType()!=null) {
					// return it as a String
					String result = EntityUtils.toString(entity2);
					entity2.getContent().close();

					logger.log(Level.INFO, "-----result-----"+ result);
					vehiculesResponse = objectMapper.readValue(result, VehiculesResponse.class);

					logger.log(Level.INFO, "----vehicule response N° Immatriculation-----" + vehiculesResponse.getImmatriculation());

				}else {
					logger.log(Level.INFO, "-----Entity Content Type Null-----");
				}

			}catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace(); 
			}

		}catch (Exception e2) {
			e2.printStackTrace();
		} 


		return vehiculesResponse;

	}



	public VehiculesResponse updateVehicule(Vehicules vehicules, String accessToken) {

		logger.log(Level.INFO, "--About to update vehicules---");

		urlBase = env.getProperty("api.dna.vehicule");

		VehiculesResponse vehiculesResponse = null;

		//String queryURL = "https://dna-core.sprint-pay.com/api/vehicules";
		String queryURL = urlBase;
		/*restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken); //"Bearer " +*/

		DefaultHttpClient httpclient;
		try {
			httpclient = ASACMinfiUtils.getDefaultHttpClient();
			HttpPut putRequest = new HttpPut(queryURL);
			putRequest.setHeader("Content-type", "application/json");
			putRequest.setHeader("Authorization", "Bearer " + accessToken);

			logger.log(Level.INFO, "---Content-type----" + putRequest.getHeaders("Content-type"));
			logger.log(Level.INFO, "----Authorization---" + putRequest.getHeaders("Authorization"));

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;

			jsonStr = objectMapper.writeValueAsString(vehicules);
			logger.log(Level.INFO, "---jsonStr----" + jsonStr);


			StringEntity entity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);


			putRequest.setEntity(entity);



			logger.log(Level.INFO, "-----putRequest.getEntity().toString()-----" + putRequest.getEntity().toString());
			logger.log(Level.INFO, "using getRequestLine(): " + putRequest.getRequestLine());
			logger.log(Level.INFO, "using getURI(): " + putRequest.getURI().toString());

			try {
				HttpResponse httpResponse = httpclient.execute(putRequest);

				logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
				logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

				logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
				logger.log(Level.INFO, "-----httpResponse.getEntity().getContentType()-----" + httpResponse.getEntity().getContentType());

				logger.log(Level.INFO, "****httpResponse.getStatusLine()******* : " + httpResponse.getStatusLine());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getReasonPhrase()---" +httpResponse.getStatusLine().getReasonPhrase());
				logger.log(Level.INFO, "----httpResponse.getStatusLine().getStatusCode()---" +httpResponse.getStatusLine().getStatusCode());
				logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

				org.apache.http.HttpEntity entity2 = httpResponse.getEntity();
				logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
				logger.log(Level.INFO, "****entity2******* : " + entity2);

				String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
				httpResponse.getEntity().getContent().close();


				logger.log(Level.INFO, "-----result-----"+ result);

				String description = httpResponse.getStatusLine().toString();

				try {
					vehiculesResponse = objectMapper.readValue(result, VehiculesResponse.class);
					logger.log(Level.INFO, "----UPDATE: vehicule response---" + vehiculesResponse.getImmatriculation());

				}catch(UnrecognizedPropertyException upe) {

					ErrorResponse errorResponse;
					ErrorResponse2 errorResponse2;

					logger.log(Level.ERROR, "----UnrecognizedPropertyException caught ---");

					try {
						errorResponse = objectMapper.readValue(result, ErrorResponse.class);
						description = errorResponse.getTitle();
					}catch(UnrecognizedPropertyException upe2) {
						errorResponse2 = objectMapper.readValue(result, ErrorResponse2.class);
						description = errorResponse2.getViolations().get(0).getField() + " : " + errorResponse2.getViolations().get(0).getMessage();

					}

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, 4, "TRANS_VEH_ERROR_"+description); // Ok la ressource est crée
					logger.log(Level.WARN, "----UPDATE: Vehicule not updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "ERROR");

					return null;

				}

				//Si l'assuré est crée
				if(httpResponse.getStatusLine().getStatusCode() == 200 || httpResponse.getStatusLine().getStatusCode() == 201 || httpResponse.getStatusLine().getStatusCode() == 202 || httpResponse.getStatusLine().getStatusCode() == 204) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, 3, "TRANS_VEH_VALID_"+description); // Ok la ressource est crée
					logger.log(Level.INFO, "----UPDATE: Vehicule updated on our side ------");

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "VALID");

				}else if(httpResponse.getStatusLine().getStatusCode() == 403 || httpResponse.getStatusLine().getStatusCode() == 404 || httpResponse.getStatusLine().getStatusCode() == 500 || httpResponse.getStatusLine().getStatusCode() == 502 || httpResponse.getStatusLine().getStatusCode() == 503 || httpResponse.getStatusLine().getStatusCode() == 504 || httpResponse.getStatusLine().getStatusCode() == 507 ){

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, -3, "TRANS_VEH_PENDING TRANSFER_"+description); //erreur, donnée à retransféré

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "PENDING TRANSFER");


				}else if(String.valueOf(httpResponse.getStatusLine().getStatusCode()).startsWith("3") || httpResponse.getStatusLine().getStatusCode() == 400 || httpResponse.getStatusLine().getStatusCode() == 401 || httpResponse.getStatusLine().getStatusCode() == 402 || httpResponse.getStatusLine().getStatusCode() == 405 || httpResponse.getStatusLine().getStatusCode() == 406 || httpResponse.getStatusLine().getStatusCode() == 501 || httpResponse.getStatusLine().getStatusCode() == 505 || httpResponse.getStatusLine().getStatusCode() == 510 || httpResponse.getStatusLine().getStatusCode() == 207 || httpResponse.getStatusLine().getStatusCode() == 206 || httpResponse.getStatusLine().getStatusCode() == 205) {

					//Mis à jour des champs c_status, c_date_transfer et comments		
					vehicules = updateVehiculesInOurBD(vehicules, 4, "TRANS_VEH_ERROR_"+description); //rejet du serveur

					//Insertion dans le Journal
					insertIntoJournal(vehicules, description, "ERROR");

				}

				logger.log(Level.INFO, "----UPDATE: vehicules response immatriculation---" + vehiculesResponse.getImmatriculation());


			} catch (ClientProtocolException e1) {

				e1.printStackTrace();
			} catch (IOException e1) {

				e1.printStackTrace(); 
			}

		} catch (Exception e2) {

			e2.printStackTrace();
		}

		if(vehiculesResponse!=null)
			logger.log(Level.INFO, "*****Vehicules Response*****" + vehiculesResponse.toString());

		return vehiculesResponse;
	}

	public Vehicules updateVehiculesInOurBD(Vehicules vehicule, int status_code, String comment) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		try {

			Date tr_date = dateFormat.parse(strDate);

			String update_assure = "update vehicule set c_status = ?, c_date_transfer = ?, commentaires = ? where immatriculation = ? ";

			Object[] params = new Object[] {status_code, tr_date, comment, vehicule.getImmatriculation()};

			int update =  jdbcTemplate.update(update_assure, params);

			logger.log(Level.INFO, "---update---" + update);

			if(update!=1) {
				return null;
			}

			vehicule = listVehicules(vehicule.getImmatriculation(),status_code).get(0);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return vehicule;

	}

	public Boolean insertIntoJournal(Vehicules vehicule, String description, String status) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		String strDate = dateFormat.format(date);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {

			Date dateInscription = dateFormat.parse(strDate);

			//String insertIntoJournal = "Insert into journal (reference, description, status, date_inscription) values (?, ?, ?, ?)";

			String insertIntoJournal = "INSERT INTO journal_vehicule (code_assure, code_assureur, marque, modele,  date_premiere_mise_circulation, double_commande, immatriculation, chassis, usage, charge_utile, " +
					"puissance_fiscale, remorque, nombre_portes, immatriculation_remorque, source_energie, nombre_de_places, cylindree, " + 
					"poids_total_autorise_en_charge, responsabilite_civile, type_engin, utilitaire,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires, j_logdate) " +
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?, ?, ?, ?, ?);";


			//String updateJournal = "Update journal set description = ?, status = ?, date_inscription = ? where reference = ?";

			try {

				//Object[] params = new Object[] {"TRANSVEH"+String.valueOf(vehicule.getVid()), description, status, dateInscription};

				Object[] params = new Object[] {
						vehicule.getCode_assure(), 
						vehicule.getCode_assureur(), 
						vehicule.getMarque(), 
						vehicule.getModele(), 
						sdf.parse(vehicule.getDate_premiere_mise_circulation()), 
						vehicule.getDouble_commande()!=null?Integer.parseInt(vehicule.getDouble_commande()):null, 
						vehicule.getImmatriculation(), 
						vehicule.getChassis(), 
						vehicule.getUsage(), 
						vehicule.getCharge_utile()!=null?Double.parseDouble(vehicule.getCharge_utile()):null,
						vehicule.getPuissance_fiscale()!=null?Integer.parseInt(vehicule.getPuissance_fiscale()):null, 
						vehicule.getRemorque(), vehicule.getNombre_portes()!=null?Integer.parseInt(vehicule.getNombre_portes()):null, 
						vehicule.getImmatriculation_remorque(), 
						vehicule.getSource_energie(),
						vehicule.getNombre_de_places()!=null?Integer.parseInt(vehicule.getNombre_de_places()):null, 
						vehicule.getCylindree()!=null?Integer.parseInt(vehicule.getCylindree()):null, 
						vehicule.getPoids_total_autorise_en_charge()!=null?Long.parseLong(vehicule.getPoids_total_autorise_en_charge()):null, 
						vehicule.getResponsabilite_civile()!=null?Integer.parseInt(vehicule.getResponsabilite_civile()):null, 
						vehicule.getType_engin()!=null?Integer.parseInt(vehicule.getType_engin()):null, 
						vehicule.getUtilitaire()!=null?Integer.parseInt(vehicule.getUtilitaire()):null, 
						vehicule.getC_status(), 
						vehicule.getC_date_creation(), 
						vehicule.getC_date_mis_a_jour(), 
						vehicule.getC_date_transfer(), 
						vehicule.getCommentaires(), 
						dateInscription};


				int insert =  jdbcTemplate.update(insertIntoJournal, params);

				logger.log(Level.INFO, "---insert---" + insert);

				if(insert!=1) {
					return Boolean.FALSE;
				}

			}catch(DuplicateKeyException dke) {

				/**Object[] params = new Object[] {description, status, dateInscription,String.valueOf(assure.getAid())};

				int update =  jdbcTemplate.update(updateJournal, params);

				logger.log(Level.INFO, "---update---" + update);

				if(update!=1) {
					return Boolean.FALSE;
				}*/
				dke.printStackTrace();

			}

		}catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Boolean.TRUE;

	}


}
