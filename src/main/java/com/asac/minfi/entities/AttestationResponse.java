package com.asac.minfi.entities;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class AttestationResponse {

	private String code_assure;
	private String code_assureur;
	private String couleur;
	private String date_echeance;
	private String date_effet;
	private String date_emission;
	private String immatriculation;
	private String numero_attestation;
	private String numero_police;
	private String statut;
	private String zone_circulation;
	private String remorque;

}
