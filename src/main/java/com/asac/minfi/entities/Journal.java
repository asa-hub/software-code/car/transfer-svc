package com.asac.minfi.entities;

import java.util.Date;


@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Journal {
	
	private Integer jid;
	private String reference;
	private String description;
	private String status;
	private Date date_inscription;
}
