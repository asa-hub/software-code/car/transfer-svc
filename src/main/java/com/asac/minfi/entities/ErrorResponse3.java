package com.asac.minfi.entities;


@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class ErrorResponse3 {

	private String codeDetail;
	private String type;
	private String title;
	private String status;
	private String message;
	private String params;
}
