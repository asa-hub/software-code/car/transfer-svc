package com.asac.minfi.entities;

import java.util.Date;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Journal_Ventes {

	private Integer jvid;
	private String code_assure;
	private String code_assureur;
	private String code_intermediaire_dna; //code_intermediaire;
	private String dta;
	private String immatriculation;
	private String numero_attestation;
	private String prime_nette_rc;
	
	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;
	private Date j_logdate;
	
}
