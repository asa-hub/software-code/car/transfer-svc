package com.asac.minfi.entities;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Journal_Sinistres {
	
	private Integer jsid;
	private String code_assureur;
	private String cause_sinistre;
	private String date_declaration;
	private String date_survenance;
	private String numero_attestation;
	private String reference;
	private String type_dommage;
	
	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;
	private Date j_logdate;
	

}
