package com.asac.minfi.entities;

import java.util.Date;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Journal_Assure {
	private Integer jaid;
	private String categorie_permis;
	private String code_assure;
	private String code_assureur;
	private String boite_postale; //code_postal;
	private String date_delivrance;
	private String date_naissance;
	private String date_naissance_conducteur;
	private String email;
	private String nom;
	private String nom_prenom_conducteur;
	private String num_contribuable; // précédemment numero_contribuable;
	private String numero_permis;
	private String permis_delivre_par;
	private String prenom;
	private String profession;
	private String qualite;
	private String rue;
	private String telephone;
	private String ville;
	
	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;
	private Date j_logdate;
		
}
