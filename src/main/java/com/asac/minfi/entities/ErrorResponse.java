package com.asac.minfi.entities;


@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class ErrorResponse {

	private String entityName;
	private String errorKey;
	private String type;
	private String title;
	private String status;
	private String message;
	private String params;
}
