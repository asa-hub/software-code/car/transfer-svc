package com.asac.minfi.entities;

@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@lombok.ToString
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode
public class Adresses {
	
	/*private String city;
	private String country;
	private String createDate;
	private String createUserId;
	private String lastUpdate;
	private String lastUpdateUserId;
	private String phone;
	private String postalCode;
	private String secondPhone;
	private String street;*/
	
	private Integer aid;
	private String boite_postal; //code_postal 				varchar(64) NULL,
	private String rue ; //						varchar(64) NOT NULL,
	private String ville 	; //					varchar(64) NOT NULL,
	private String pays 	; //					varchar(64) NOT NULL,
	private String telephone 	; //				numeric NULL,
	private String email 		; //				varchar(64) NULL,
	private String web 				;//		varchar(64) NULL,
	private String c_status				; //	NUMERIC NOT NULL DEFAULT 0,
	private String c_date_creation		; //		timestamp NOT NULL DEFAULT NOW(),
	private String c_date_mis_a_jour	; //		timestamp NULL,
	private String c_date_transfer			;//	timestamp NULL
	private String commentaires	;
	
	
}
