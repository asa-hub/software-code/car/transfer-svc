package com.asac.minfi.entities;

import java.util.List;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class ErrorResponse2 {

	private String type;
	private String title;
	private String status;
	private String path;
	private List<Violations> violations;
	private String message;
	
}
