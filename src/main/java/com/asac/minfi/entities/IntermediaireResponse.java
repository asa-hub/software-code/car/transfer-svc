package com.asac.minfi.entities;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class IntermediaireResponse {
	
	private Integer iid;
	private String boite_postale;
	private String code_intermediaire_dna; //identifiant_dna;
	private String nom_intermediaire;
	private String num_contribuable;
	private String telephone;
	private String type_intermediaire;
	private String ville;
	private String statut_intermediaire;
	private String date_debut_suspension;
	private String date_fin_suspension;

}
