package com.asac.minfi.entities;

import java.util.Date;

@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class Vehicules {

	private Integer vid;
	private String charge_utile = "8";
	private String chassis = "0";
	private String code_assure;
	private String code_assureur;
	private String date_premiere_mise_circulation = "2000";
	private String double_commande = "1";
	private String immatriculation = "LT585WS";
	private String immatriculation_remorque; //immatriculation_semi_remorque = "CE 5524";
	private String marque = "Toyota";
	private String modele = "Yaris";
	private String cylindree; //nombre_de_cylindres = "22";
	private String nombre_de_places = "8";
	private String nombre_portes = "4";
	//private String poids = "3.5";
	private String poids_total_autorise_en_charge = "3.5";
	private String puissance_fiscale = "7";
	private String responsabilite_civile = "1";
	private String remorque;//semi_remorque = "0";
	private String source_energie = "ESSENCE";
	private String type_engin = "1";
	private String usage = "I,II,X";
	private String utilitaire = "1";
	
	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;


}
