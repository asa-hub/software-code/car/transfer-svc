package com.asac.minfi.entities;

import java.util.Date;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class AssureResponse {
	private String categorie_permis;
	private String code_assure;
	private String code_assureur;
	private String boite_postale;
	private String date_delivrance;
	private String date_naissance;
	private String date_naissance_conducteur;
	private String email;
	private String nom;
	private String nom_prenom_conducteur;
	private String num_contribuable;
	private String numero_permis;
	private String permis_delivre_par;
	private String prenom;
	private String profession;
	private String qualite;
	private String rue;
	private String telephone;
	private String ville;
	
}
