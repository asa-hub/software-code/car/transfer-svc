package com.asac.minfi.entities;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class VentesResponse {

	private String code_assure;
	private String code_intermediaire_dna;
	private String dta;
	private String immatriculation;
	private String numero_attestation;
	private String prime_nette_rc;
	private String code_assureur;

	
}
