package com.asac.minfi.entities;

@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class VehiculesResponse {

	private String charge_utile = "8";
	private String chassis = "0";
	private String code_assure;
	private String code_assureur;
	private String date_premiere_mise_circulation = "2000";
	private String double_commande = "1";
	private String immatriculation = "LT585WS";
	private String immatriculation_remorque = "CE 5524";
	private String marque = "Toyota";
	private String modele = "Yaris";
	private String cylindree = "22";
	private String nombre_de_places = "8";
	private String nombre_portes = "4";
	private String poids_total_autorise_en_charge = "3.5";
	private String puissance_fiscale = "7";
	private String responsabilite_civile = "1";
	private String remorque = "0";
	private String source_energie = "ESSENCE";
	private String type_engin = "1";
	private String usage = "I,II,X";
	private String utilitaire = "1";


}
