package com.asac.minfi.entities;

import java.util.Date;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Attestation {

	private Integer aid;
	private String code_assure;
	private String code_assureur;
	private String couleur;
	private String date_echeance;
	private String date_effet;
	private String date_emission;
	private String immatriculation;
	private String numero_attestation;
	private String numero_police;
	private String statut;
	private String zone_circulation;
	private String remorque;
	
	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;


}
