package com.asac.minfi.entities;


@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Violations {

	private String field;
	private String message;
}
