package com.asac.minfi.entities;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class RemorquesResponse {

	private String code_assure;
	private String code_assureur;
	private String immatriculation_remorque;
	
}
