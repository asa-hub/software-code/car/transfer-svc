package com.asac.minfi.entities;

public class Columns {
     
     private String Id = "Id";
	 private String eventDate = "Event Date";
	 private String notificationDate = "Notification Date";
	 private String title = "Title";
	 private String description = "Description";
	 
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	 
	 
}
