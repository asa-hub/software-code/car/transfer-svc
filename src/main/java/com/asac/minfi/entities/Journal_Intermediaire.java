package com.asac.minfi.entities;

import java.util.Date;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Journal_Intermediaire {

	private Integer jiid;
	private String boite_postal;
	private String code_intermediaire_dna; //identifiant_dna;
	private String nom_intermediaire;
	private String num_contribuable;
	private String telephone;
	private String type_assureur;
	private String ville;

	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;
	private Date j_logdate;

}
