package com.asac.minfi.entities;

import java.util.Date;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
public class Journal_Remorques {

	private Integer jrid;
	private String code_assure;
	private String code_assureur;
	private String immatriculation_remorque;
	
	private int c_status;
	private Date c_date_creation;
	private Date c_date_mis_a_jour;
	private Date c_date_transfer;
	private String commentaires	;
	private Date j_logdate;
	
}
