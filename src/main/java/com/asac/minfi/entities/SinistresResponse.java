package com.asac.minfi.entities;


@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class SinistresResponse {
	
	/***private String id;
	private String enventDate;
	private String notificationDate;
	private String title;
	private String description;
	
	private String createDate;
	private String lastUpdate;
	private String createUserId;
	
	private String lastUpdateUserId;
	private Columns columns;
	
	private String numero_attestation;
	private String type_dommage;
	private String cause_sinistre;
	private String date_survenance;
	private String date_declaration;
	private String code_assureur;
	private String reference;*/
	
	private String code_assureur;
	private String cause_sinistre;
	private String date_declaration;
	private String date_survenance;
	private String numero_attestation;
	private String reference;
	private String type_dommage;

	
	

}
