package com.asac.minfi.entities;

/**@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@lombok.ToString
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode*/
public class AdressesResponse {
	
	private String city;
	private String country;
	private String createDate;
	private String createUserId;
	private String id;
	private String lastUpdate;
	private String lastUpdateUserId;
	private String phone;
	private String postalCode;
	private String secondPhone;
	private String street;
	
	
	
	public AdressesResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AdressesResponse(String city, String country, String createDate, String createUserId, String lastUpdate,
			String lastUpdateUserId, String phone, String postalCode, String secondPhone, String street) {
		super();
		this.city = city;
		this.country = country;
		this.createDate = createDate;
		this.createUserId = createUserId;
		this.lastUpdate = lastUpdate;
		this.lastUpdateUserId = lastUpdateUserId;
		this.phone = phone;
		this.postalCode = postalCode;
		this.secondPhone = secondPhone;
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getLastUpdateUserId() {
		return lastUpdateUserId;
	}
	public void setLastUpdateUserId(String lastUpdateUserId) {
		this.lastUpdateUserId = lastUpdateUserId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getSecondPhone() {
		return secondPhone;
	}
	public void setSecondPhone(String secondPhone) {
		this.secondPhone = secondPhone;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	
	
	
}
