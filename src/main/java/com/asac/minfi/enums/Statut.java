package com.asac.minfi.enums;

import java.util.ArrayList;
import java.util.List;

public enum Statut {
	
	EN_STOCK("EN STOCK"),
	VENDUE("VENDUE"),
	MISE_AU_REBUT("MISE AU REBUT"),
	ANNULEE("ANNULEE"),
	RESILIEE("RESILIEE"),
	SUSPENDUE("SUSPENDUE"),
	REMISE_EN_VIGUEUR("REMISE EN VIGUEUR");
	
	/**
	 * La valeur du statut
	 */
	private final String value;
	

	/**
	 * Liste statut
	 */
	private static final List<Statut> types = new ArrayList<Statut>();
	
	/**
	 * constructeur
	 */
	static{
		
	 types.add(EN_STOCK);
	 types.add(VENDUE);
	 types.add(MISE_AU_REBUT);
	 types.add(ANNULEE);
	 types.add(RESILIEE);
	 types.add(SUSPENDUE);
	 types.add(REMISE_EN_VIGUEUR);

	}
	
	/**
	 * 
	 * @param value the value to set
	 */
	private Statut(String value){
		
		this.value = value;
	}
	
    /**
     * 
     * @return the value
     */
	public String value() {
        return value;
    }

	/**
	 * 
	 * @return the value
	 */
	public String getValue() {
        return value;
    }
	
	/**
	 * 
	 * @return the types
	 */
	public static List<Statut> types(){
		
		return types;
		
	}

}
