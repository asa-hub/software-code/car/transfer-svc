package com.asac.minfi.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@lombok.Getter
@lombok.Setter

@Component
public class Configurations {

	@Value("${api.dna.assure}")
	private String assureBase;
	
	@Value("${api.dna.vehicule}")
	private String vehiculeBase;
	
	@Value("${api.dna.sinistre}")
	private String sinistreBase;
	
	@Value("${api.dna.attestation}")
	private String attestationBase;
	
	@Value("${api.dna.vente}")
	private String venteBase;
	
	@Value("${api.dna.intermediaire}")
	private String intermediaireBase;
	
	@Value("${api.dna.remorque}")
	private String remorqueBase;
}
