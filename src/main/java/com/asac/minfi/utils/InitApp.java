package com.asac.minfi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import com.asac.minfi.services.interfaces.AsyncService;

@Component
@EnableAsync
public class InitApp {
	
	@Autowired
	AsyncService asyncService;
	
	private Logger logger = LogManager.getLogger(InitApp.class);
	
	private TimerTask task;

	private Timer timer;
	
	@Autowired
	private Environment env;
	
	private boolean firstTime = true;
	
	@Autowired
	AppInitialiser app;


	
	public void robot(){
		try{

			if(task != null )task.cancel();
			if(timer != null )timer.cancel();

			task = new TimerTask(){
				@Override
				public void run(){
					try{

						logger.log(Level.INFO, "--------------------In robot() demon--------------------");
						
						logger.log(Level.INFO, "**********************Robot Create Assurés*************************");
						asyncService.createAssures();

						logger.log(Level.INFO, "**********************Robot Create Vehicules***********************");
						asyncService.createVehicules();
						
						logger.log(Level.INFO, "**********************Robot Create Remorques***********************");
						asyncService.createRemorquess();

						logger.log(Level.INFO, "**********************Robot Update Attestations********************");
						asyncService.updateAttestations();

						logger.log(Level.INFO, "**********************Robot Create Ventes**************************");
						asyncService.createVentess();

						logger.log(Level.INFO, "**********************Robot Create Sinistres***********************");
						asyncService.createSinistres();

					}catch (InterruptedException e) {
						System.out.println("-----Interrupted Exception------" + e.getMessage());
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
				}	
			};

			timer = new java.util.Timer(true);

			//int sec = 60 ;int min = 60;int hour = 24;
			int sec = Integer.parseInt(env.getProperty("exec_seconds_robot"));
			int min = Integer.parseInt(env.getProperty("exec_min_robot"));
			int hour = Integer.parseInt(env.getProperty("exec_heure_robot"));


			timer.schedule(task, getRobotTime(), hour*min*sec*1000); //chaque jour  //DateUtils.addMinutes(new Date(), 1)
			//timer.schedule(task, new Date(), hour*min*sec*1000); //chaque jour  //DateUtils.addMinutes(new Date(), 1)


		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void robot2(){
		try{

			if(task != null )task.cancel();
			if(timer != null )timer.cancel();

			task = new TimerTask(){
				@Override
				public void run(){
					try{

						logger.log(Level.INFO, "--------------------In robot() demon--------------------");

						logger.log(Level.INFO, "**********************Robot Create Assurés*************************");
						app.createAssures();

						logger.log(Level.INFO, "**********************Robot Create Vehicules***********************");
						app.createVehiculess();

						logger.log(Level.INFO, "**********************Robot Create Remorques***********************");
						app.createRemorquess();

						logger.log(Level.INFO, "**********************Robot Update Attestations********************");
						app.updateAttestations();

						logger.log(Level.INFO, "**********************Robot Create Ventes**************************");
						app.createVentess();

						logger.log(Level.INFO, "**********************Robot Create Sinistres***********************");
						app.createSinistres();

						///logger.log(Level.INFO, "****Robot Update Intermédiaires*****");
						///updateIntermediaires();

					}catch(Exception e){e.printStackTrace();}
				}	
			};

			timer = new java.util.Timer(true);

			//int sec = 60 ;int min = 60;int hour = 24;
			int sec = Integer.parseInt(env.getProperty("exec_seconds_robot"));
			int min = Integer.parseInt(env.getProperty("exec_min_robot"));
			int hour = Integer.parseInt(env.getProperty("exec_heure_robot"));


			timer.schedule(task, getRobotTime(), hour*min*sec*1000); //chaque jour  //DateUtils.addMinutes(new Date(), 1)
			//timer.schedule(task, new Date(), hour*min*sec*1000); //chaque jour  //DateUtils.addMinutes(new Date(), 1)


		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	public Date getRobotTime() {

		Date date = null;

		Calendar executeRobot = Calendar.getInstance();

		String hour = env.getProperty("heure_robot");
		String min = env.getProperty("minute_robot");
		int heure = 23 ; int mins = 0;

		try {
			heure = Integer.parseInt(hour);
			mins = Integer.parseInt(min);
		}catch(NumberFormatException nfe) {
			nfe.printStackTrace();
		}

		executeRobot.set(Calendar.HOUR_OF_DAY,heure);
		executeRobot.set(Calendar.MINUTE, mins); //Integer.parseInt(heureMAJStat.split(":")[1])
		//executeRobot.set(Calendar.SECOND, 0);
		//executeRobot.set(Calendar.MILLISECOND, 0);

		date = executeRobot.getTime();

		try {

			if(date.before(new Date())){

				if(firstTime) {
					date = new Date();
					logger.log(Level.INFO, "******Will Execute******** now " + date);
					firstTime=false;
				}else {

					date = DateUtils.addDays(date, 1);

					logger.log(Level.INFO, "Executed once already, and will be scheduled for " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(DateFormatUtils.format(date, "dd/MM/yyyy HH:mm:ss")));
				}
			}
			else{
				if(firstTime) {
					date = new Date();
					logger.log(Level.INFO, "******Will Execute******** now " + date);
					firstTime=false;
				}
				logger.log(Level.INFO, "******Will Execute******** on " + date);

			}

			logger.log(Level.INFO, "---NEXT TIME TO RUN---" + date);



		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}
	
	@PostConstruct
	public void launchRobot() {
		
		//Déversements en parallèle
		if(env.getProperty("robot.deversement.parallel").equals("1")) {
			logger.info("-----------------PARALLELE-------------------------");
			robot();
		} //Déversements en séquentiel
		else if(env.getProperty("robot.deversement.parallel").equals("0")) {
			logger.info("-----------------SEQUENTIEL-------------------------");
			robot2();
		}
	}

}
