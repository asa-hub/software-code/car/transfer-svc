package com.asac.minfi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.Attestation;
import com.asac.minfi.entities.AttestationResponse;
import com.asac.minfi.entities.Intermediaire;
import com.asac.minfi.entities.IntermediaireResponse;
import com.asac.minfi.entities.Remorques;
import com.asac.minfi.entities.RemorquesResponse;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.entities.SinistresResponse;
import com.asac.minfi.entities.Vehicules;
import com.asac.minfi.entities.VehiculesResponse;
import com.asac.minfi.entities.Ventes;
import com.asac.minfi.entities.VentesResponse;
import com.asac.minfi.services.interfaces.AssureService;
import com.asac.minfi.services.interfaces.AsyncService;
import com.asac.minfi.services.interfaces.AttestationsService;
import com.asac.minfi.services.interfaces.IntermediaireService;
import com.asac.minfi.services.interfaces.RemorquesService;
import com.asac.minfi.services.interfaces.SinistresService;
import com.asac.minfi.services.interfaces.VehiculesService;
import com.asac.minfi.services.interfaces.VentesService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Classe pour l'initialisation de l'application - gestion des robots chargés de transférer les entités
 * @author Stephane Mouafo
 *
 */
@Component
public class AppInitialiser {

	private TimerTask task;

	private Timer timer;

	@Autowired
	private Environment env;

	@Autowired
	AssureService assureService;

	@Autowired
	VehiculesService vehiculeService;

	@Autowired
	SinistresService sinistresService;

	@Autowired
	VentesService ventesService;

	@Autowired
	AttestationsService attestationService;

	@Autowired
	IntermediaireService intermediaireService;

	@Autowired
	RemorquesService remorquesService;

	

	private Logger logger = LogManager.getLogger(AppInitialiser.class);

	private boolean firstTime = true;


	public ResponseEntity<Object> createAssures() {
		

		logger.log(Level.INFO, "----------------From Robot : In method createAssures()---------------");

		List<Assure> assures = assureService.listAssure();
		List<AssureResponse> assureResponses = new ArrayList<AssureResponse>();

		AssureResponse assureResponse ;
		for(Assure a: assures) {
			
            ///logger.info("################################################### Calling async createAssure for " + a.getAid() + "-" + a.getNom() + "-" + a.getPrenom() + " active count: " + taskExecutor.getActiveCount()+ " , Pool size: " + taskExecutor.getPoolSize()+ " , Queue Size: " + taskExecutor.getThreadPoolExecutor().getQueue().size());

			assureResponse = assureService.createAssure(a);
			if(assureResponse==null) {
				logger.log(Level.ERROR, "----------There was an error processing the assure " + a.getNom() +" , check the assure and journal tables-----------------");
			}else {
				assureResponses.add(assureResponse);
			}
		}

		return  new ResponseEntity<>(assureResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> createVehiculess() {

		logger.log(Level.INFO, "------------------------From Robot : In createVehiculess()-------------------");

		List<Vehicules> vehicules = vehiculeService.listVehicules();
		List<VehiculesResponse> vehiculeResponses = new ArrayList<VehiculesResponse>();

		VehiculesResponse vehiculeResponse ;
		for(Vehicules a: vehicules) {
			vehiculeResponse = vehiculeService.createVehicules(a);
			if(vehiculeResponse==null) {
				logger.log(Level.ERROR, "-----------Associated Assuré does not exists or there was an error processing the vehicule " + a.getImmatriculation() +" , check the vehicule and journal tables-------------------");
			}else {
				vehiculeResponses.add(vehiculeResponse);
			}
		}

		return  new ResponseEntity<>(vehiculeResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> createSinistres() {

		logger.log(Level.INFO, "-----------------------From Robot : In createSinistres() ------------------------");


		List<Sinistres> sinistres = sinistresService.listSinistres();
		List<SinistresResponse> sinistreResponses = new ArrayList<SinistresResponse>();


		SinistresResponse sinistresResponse;
		for(Sinistres s: sinistres) {
			sinistresResponse = sinistresService.createSinistres(s);
			if(sinistresResponse==null) {
				logger.log(Level.ERROR, "--------------------There was an error processing the sinistre with reference " + s.getReference() +" , check the sinistre and journal tables-------------------");
			}else {
				sinistreResponses.add(sinistresResponse);
			}
		}

		return  new ResponseEntity<>(sinistreResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> createVentess() {

		logger.log(Level.INFO, "-----------------From Robot : In createVentess() ------------------------");

		List<Ventes> ventess = ventesService.listVentes();
		List<VentesResponse> ventesResponses = new ArrayList<VentesResponse>();

		VentesResponse ventesResponse ;
		for(Ventes a: ventess) {
			ventesResponse = ventesService.createVentes(a);
			if(ventesResponse==null) {
				logger.log(Level.ERROR, "----------There was an error processing the vente with the vente of attestation " + a.getNumero_attestation() +" , check the vente and journal tables---------------");
			}else {
				ventesResponses.add(ventesResponse);
			}
		}

		return  new ResponseEntity<>(ventesResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> createAttestations() {

		logger.log(Level.INFO, "-----------------------From Robot : In createAttestations()---------------------------");

		List<Attestation> attestations = attestationService.listAttestation();
		List<AttestationResponse> attestationResponses = new ArrayList<AttestationResponse>();

		AttestationResponse attestationResponse ;
		for(Attestation a: attestations) {
			attestationResponse = attestationService.createAttestation(a);
			if(attestationResponse==null) {
				logger.log(Level.ERROR, "-----------------There was an error processing the attestation " + a.getNumero_attestation() +" , check the attestation and journal tables---------------");
			}else {
				attestationResponses.add(attestationResponse);
			}
		}

		return  new ResponseEntity<>(attestationResponses, HttpStatus.OK);

	}


	public ResponseEntity<Object> updateAttestations(){

		logger.log(Level.INFO, "-------------------------From Robot : In updateAttestations()--------------------");


		List<Attestation> attestations = attestationService.listAttestation();
		List<AttestationResponse> attestationResponses = new ArrayList<AttestationResponse>();

		AttestationResponse attestationResponse = null;
		for(Attestation a: attestations) {
			attestationResponse = attestationService.updateAttestation(a);
			if(attestationResponse==null) {
				logger.log(Level.ERROR, "-------------------There was an error processing the attestation " + a.getNumero_attestation() +" , check the attestation and journal tables-------------");
			}else {
				attestationResponses.add(attestationResponse);
			}
		}

		return  new ResponseEntity<>(attestationResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> createIntermediaires() { 

		logger.log(Level.INFO, "--------------------From Robot : In createIntermediaires()-----------------------");


		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire();
		List<IntermediaireResponse> intermediaireResponses = new ArrayList<IntermediaireResponse>();

		IntermediaireResponse intermediaireResponse ;
		for(Intermediaire a: intermediaires) {
			intermediaireResponse = intermediaireService.createIntermediaire(a);
			if(intermediaireResponse==null) {
				logger.log(Level.ERROR, "--------------There was an error processing the intermediaire with code " + a.getCode_intermediaire_dna() +" , check the intermediaire and journal tables---------------");
			}else {
				intermediaireResponses.add(intermediaireResponse);
			}
		}

		return  new ResponseEntity<>(intermediaireResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> updateIntermediaires() { 

		logger.log(Level.INFO, "-----------------------From Robot : In updateIntermediaires()---------------------");


		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire();
		List<IntermediaireResponse> intermediaireResponses = new ArrayList<IntermediaireResponse>();

		IntermediaireResponse intermediaireResponse ;
		for(Intermediaire a: intermediaires) {
			intermediaireResponse = intermediaireService.updateIntermediaire(a);
			if(intermediaireResponse==null) {
				logger.log(Level.ERROR, "-------------There was an error processing the intermediaire with code " + a.getCode_intermediaire_dna() +" , check the intermediaire and journal tables-------------------");
			}else {
				intermediaireResponses.add(intermediaireResponse);
			}
		}

		return  new ResponseEntity<>(intermediaireResponses, HttpStatus.OK);

	}

	public ResponseEntity<Object> createRemorquess() {

		logger.log(Level.INFO, "-----------------------------From Robot : In createRemorquess()--------------------");

		List<Remorques> remorquess = remorquesService.listRemorques();
		List<RemorquesResponse> remorquesResponses = new ArrayList<RemorquesResponse>();

		RemorquesResponse remorquesResponse ;
		for(Remorques a: remorquess) {
			remorquesResponse = remorquesService.createRemorques(a);
			if(remorquesResponse==null) {
				logger.log(Level.ERROR, "-----------------------There was an error processing the remorque " + a.getImmatriculation_remorque() +" , check the remorque and journal tables----------------------");
			}else {
				remorquesResponses.add(remorquesResponse);
			}
		}

		return  new ResponseEntity<>(remorquesResponses, HttpStatus.OK);

	}

	
	public void robot(){
		try{

			if(task != null )task.cancel();
			if(timer != null )timer.cancel();

			task = new TimerTask(){
				@Override
				public void run(){
					try{

						logger.log(Level.INFO, "--------------------In robot() demon--------------------");

						logger.log(Level.INFO, "**********************Robot Create Assurés*************************");
						createAssures();

						logger.log(Level.INFO, "**********************Robot Create Vehicules***********************");
						createVehiculess();

						logger.log(Level.INFO, "**********************Robot Create Remorques***********************");
						createRemorquess();

						logger.log(Level.INFO, "**********************Robot Update Attestations********************");
						updateAttestations();

						logger.log(Level.INFO, "**********************Robot Create Ventes**************************");
						createVentess();

						logger.log(Level.INFO, "**********************Robot Create Sinistres***********************");
						createSinistres();

						///logger.log(Level.INFO, "****Robot Update Intermédiaires*****");
						///updateIntermediaires();

					}catch(Exception e){e.printStackTrace();}
				}	
			};

			timer = new java.util.Timer(true);

			//int sec = 60 ;int min = 60;int hour = 24;
			int sec = Integer.parseInt(env.getProperty("exec_seconds_robot"));
			int min = Integer.parseInt(env.getProperty("exec_min_robot"));
			int hour = Integer.parseInt(env.getProperty("exec_heure_robot"));


			timer.schedule(task, getRobotTime(), hour*min*sec*1000); //chaque jour  //DateUtils.addMinutes(new Date(), 1)
			//timer.schedule(task, new Date(), hour*min*sec*1000); //chaque jour  //DateUtils.addMinutes(new Date(), 1)


		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public Date getRobotTime() {


		Date date = null;

		Calendar executeRobot = Calendar.getInstance();

		String hour = env.getProperty("heure_robot");
		String min = env.getProperty("minute_robot");
		int heure = 23 ; int mins = 0;

		try {
			heure = Integer.parseInt(hour);
			mins = Integer.parseInt(min);
		}catch(NumberFormatException nfe) {
			nfe.printStackTrace();
		}

		executeRobot.set(Calendar.HOUR_OF_DAY,heure);
		executeRobot.set(Calendar.MINUTE, mins); //Integer.parseInt(heureMAJStat.split(":")[1])
		//executeRobot.set(Calendar.SECOND, 0);
		//executeRobot.set(Calendar.MILLISECOND, 0);

		date = executeRobot.getTime();

		try {

			if(date.before(new Date())){

				if(firstTime) {
					date = new Date();
					logger.log(Level.INFO, "******Will Execute******** now " + date);
					firstTime=false;

				}else {

					date = DateUtils.addDays(date, 1);

					logger.log(Level.INFO, "Executed once already, and will be scheduled for " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(DateFormatUtils.format(date, "dd/MM/yyyy HH:mm:ss")));
				}
			}
			else{
				if(firstTime) {
					date = new Date();
					logger.log(Level.INFO, "******Will Execute******** now " + date);
					firstTime=false;
				}
				logger.log(Level.INFO, "******Will Execute******** on " + date);

			}

			logger.log(Level.INFO, "---NEXT TIME TO RUN---" + date);



		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}



	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	/*@PostConstruct
	public void launchRobot() {

		///robot();
		//startRobots();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

}
