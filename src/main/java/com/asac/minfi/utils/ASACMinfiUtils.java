package com.asac.minfi.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.asac.minfi.entities.AuthentificationResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ASACMinfiUtils {

	private Connection conn;

	@Autowired
	private static Environment env;

	private static Logger logger = LogManager.getLogger(AppInitialiser.class);

	private static Map<String, String> props = new HashMap<String, String>();

	private static AuthentificationResponse currentAuthResp = null;

	private static Date lastDateAuthResp = null;


	public static DefaultHttpClient getDefaultHttpClient() throws Exception 
	{
		DefaultHttpClient httpClient = new DefaultHttpClient();
		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		ssl_ctx.init(null, certs, new SecureRandom());
		SSLSocketFactory ssf = new SSLSocketFactory(ssl_ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		ClientConnectionManager ccm = httpClient.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme("https", 443, ssf));
		return new DefaultHttpClient(ccm, httpClient.getParams());
	}

	public static AuthentificationResponse getAuthentified() {

		logger.log(Level.INFO, "-----------Authentifying---------");

		AuthentificationResponse authResp = null;

		if(currentAuthResp!=null && lastDateAuthResp!=null) {
			Date d = Calendar.getInstance().getTime();
			Long currentTime = d.getTime();
			logger.log(Level.INFO, "-----------currentTime---------" + d + " ----long:-----" + currentTime);

			Long oldTime = lastDateAuthResp.getTime();
			logger.log(Level.INFO, "------------oldTime---------" + lastDateAuthResp + "-----long:-------"+ oldTime);

			Long diff = currentTime - oldTime;
			
			Long expiry = (3L * 60 * 1000); //3 minutes
			logger.info("-----Expiry------" + expiry);

			if(diff < expiry) { //moins de 3 minutes
				authResp = currentAuthResp;
				logger.log(Level.INFO, "------------Auth time diff is ---------" + diff + " and Auth Resp is  " + authResp.getAccess_token());
			}else {
				authResp = null;
				logger.info("---------------Auth has become null and will be changed because auth time diff is greater than 3s------------------");
			}
		}

		//String queryAuthURL = "https://dna-uaa.sprint-pay.com/oauth/token";

		String queryAuthURL = "";


		if(props==null || props.isEmpty()) {

			try (InputStream input = new FileInputStream(System.getProperty("user.dir") + File.separator + "application.properties")) {

				Properties prop = new Properties();

				// load a properties file
				prop.load(input);



				prop.forEach((key, value) -> {
					//props.put(key.toString(), value.toString());

					///if(key.equals("api.dna.authUrl")){
					//System.out.println("Key : " + key + ", Value : " + value);
					props.put(key.toString(), value.toString());
					///}


				});

				logger.log(Level.INFO, "Props size : " + props.size());


			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		/*restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.setBasicAuth("d2ViX2FwcDpBSkRLREhKS0hES1NKS0RKSERKR01RU0lPVUpES1NKRE1MQERLSlNES0FLREhTSkRIU0RMTlNOSktIRCNeQERLS1NES0xTREtKU0xLREpLU0pES0xTSkRLU0pES1NKREtTSkQ=");
		 */

		if(authResp==null) {

			try {

				DefaultHttpClient httpclient;
				try {
					httpclient = ASACMinfiUtils.getDefaultHttpClient();
					queryAuthURL = props.get("api.dna.authUrl");
					HttpPost postRequest = new HttpPost(queryAuthURL);
					postRequest.setHeader("Content-type", "application/x-www-form-urlencoded");
					//postRequest.setHeader("Authorization", "Basic d2ViX2FwcDpBSkRLREhKS0hES1NKS0RKSERKR01RU0lPVUpES1NKRE1MQERLSlNES0FLREhTSkRIU0RMTlNOSktIRCNeQERLS1NES0xTREtKU0xLREpLU0pES0xTSkRLU0pES1NKREtTSkQ=");
					postRequest.setHeader("Authorization", props.get("api.dna.authorization"));

					//logger.log(Level.INFO, "---Content-type----" + postRequest.getHeaders("Content-type"));
					//logger.log(Level.INFO, "----Authorization---" + postRequest.getHeaders("Authorization"));


					/**JSONObject bodyJsonObject = new JSONObject();
				bodyJsonObject.put("grant_type", "password");
				bodyJsonObject.put("username", "survie");
				bodyJsonObject.put("password", "survie123");
				bodyJsonObject.put("scope", "openid");*/

					List<NameValuePair> urlParameters = new ArrayList<>();
					/*urlParameters.add(new BasicNameValuePair("grant_type", "password"));
				urlParameters.add(new BasicNameValuePair("username", "survie"));
				urlParameters.add(new BasicNameValuePair("password", "survie123"));
				urlParameters.add(new BasicNameValuePair("scope", "openid"));*/

					urlParameters.add(new BasicNameValuePair("grant_type", props.get("api.dna.grantType")));
					urlParameters.add(new BasicNameValuePair("username", props.get("api.dna.username")));
					urlParameters.add(new BasicNameValuePair("password", props.get("api.dna.password")));
					urlParameters.add(new BasicNameValuePair("scope", props.get("api.dna.scope")));


					postRequest.setEntity(new UrlEncodedFormEntity(urlParameters));




					/**StringEntity stringEntity;
				//try {
					stringEntity = new StringEntity(bodyJsonObject.toString(), ContentType.APPLICATION_FORM_URLENCODED);

					postRequest.setEntity(stringEntity);*/


					//logger.log(Level.INFO, "-----postRequest.getEntity().toString()-----" + postRequest.getEntity().toString());
					//logger.log(Level.INFO, "using getRequestLine(): " + postRequest.getRequestLine());
					//logger.log(Level.INFO, "using getURI(): " + postRequest.getURI().toString());

					try {
						HttpResponse httpResponse = httpclient.execute(postRequest);

						//logger.log(Level.INFO, "---header Content-type----- "+ httpResponse.getHeaders("Content-type"));
						//logger.log(Level.INFO, "---header Authorization----- "+ httpResponse.getHeaders("Authorization"));

						//logger.log(Level.INFO, "-----httpResponse.getEntity().getContent();-----" + httpResponse.getEntity().getContent());
						logger.log(Level.INFO, "-----entity content type-----" + httpResponse.getEntity().getContentType());

						logger.log(Level.INFO, "***response status line******* : " + httpResponse.getStatusLine());
						logger.log(Level.INFO, "----response status line reason---" +httpResponse.getStatusLine().getReasonPhrase());
						logger.log(Level.INFO, "----responose status code---" +httpResponse.getStatusLine().getStatusCode());
						//logger.log(Level.INFO, "****httpResponse.getParams()******* : " + httpResponse.getParams());

						org.apache.http.HttpEntity entity = httpResponse.getEntity();
						//logger.log(Level.INFO, "****httpResponse******* : " + httpResponse.toString());
						//logger.log(Level.INFO, "****entity******* : " + entity);

						String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
						//logger.log(Level.INFO, "-----result----" + result);
						httpResponse.getEntity().getContent().close();

						//authResp = gson.fromJson(result, AuthentificationResponse.class);

						ObjectMapper objectMapper = new ObjectMapper();

						authResp = objectMapper.readValue(result, AuthentificationResponse.class);
						
						currentAuthResp = authResp;
						lastDateAuthResp = new Date();

						logger.log(Level.INFO, "-------Access token collected--------"); //+ authResp.getAccess_token());

					} catch (ClientProtocolException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} finally {
						httpclient.close();
					}

					/*} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} //new DefaultHttpClient();







				/*JSONObject bodyJsonObject = new JSONObject();
			bodyJsonObject.put("grant_type", "password");
			bodyJsonObject.put("username", "survie");
			bodyJsonObject.put("password", "survie123");
			bodyJsonObject.put("scope", "openid");
			//bodyJsonObject.put("Content-Type", "application/json;charset=UTF-8");


			HttpEntity<String> request = new HttpEntity<String>(bodyJsonObject.toString(), headers);
			logger.log(Level.INFO, "----bodyJsonObject----" + bodyJsonObject.toString());
			logger.log(Level.INFO, "---request header---" + request.getHeaders());
			logger.log(Level.INFO, "---request body---" + request.getBody());


			RestTemplateWithoutSSL rtwssl = new RestTemplateWithoutSSL();
			try {
				restTemplate = rtwssl.restTemplate();

				logger.log(Level.INFO, "----restTemplate.getRequestFactory().toString()----" +restTemplate.getRequestFactory().toString());
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			authResp = restTemplate.postForObject(queryAuthURL, request, AuthentificationResponse.class); */

			}catch(Exception je) {
				je.printStackTrace();
			}
			
			
			logger.log(Level.INFO, "------------New Auth is -----------" + authResp.getAccess_token());

		}

		return authResp;

	}


	public void openConnection(){

		try{



			//String cbsName = env.getProperty("cbs.name");
			//String cbsDescription = prop.getProperty("cbs.description");
			String cbsProviderClassName = env.getProperty("spring.datasource.driver-class-name");
			String cbsConnectionString = env.getProperty("spring.datasource.url");
			String cbsUsername = env.getProperty("spring.datasource.username");
			String cbsPassword = env.getProperty("spring.datasource.password");


			Class c = Class.forName(cbsProviderClassName);
			//LOGGER.log(Level.INFO, "-----provider class -------" + c.toString());

			Object o = Class.forName(cbsProviderClassName).newInstance();
			//LOGGER.log(Level.INFO,"****Class Object****" + o);
			//LOGGER.log(Level.INFO,"***Trying to connect to Oracle DB");
			this.conn = DriverManager.getConnection(cbsConnectionString, cbsUsername, cbsPassword);

			//LOGGER.log(Level.INFO,"***conn state *****" + this.conn);
			logger.log(Level.INFO, "***Connection to DB successful *****");



		}catch (Exception e) {
			// TODO: handle exception
		}

	}

	private  void closeConnection(Connection connection) throws Exception {
		this.conn = connection;
		if(this.conn != null) this.conn.close();
	}

}
