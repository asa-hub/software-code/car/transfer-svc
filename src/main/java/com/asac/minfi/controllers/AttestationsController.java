package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Attestation;
import com.asac.minfi.entities.AttestationResponse;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.AttestationsService;

@RestController
@RequestMapping("/attestation")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AttestationsController {

	@Autowired
	private AttestationsService attestationService;
	
	private Logger logger = LogManager.getLogger(AttestationsController.class);


	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createAttestations() { ////@RequestBody Attestation attestation

		logger.log(Level.INFO, "-----In attestation controller---");

		//Attestation attestation = getAttestationFromBD();
		//logger.log(Level.INFO, "----Attestation selected---" + attestation.getNumero_contribuable());

		List<Attestation> attestations = attestationService.listAttestation();
		List<AttestationResponse> attestationResponses = new ArrayList<AttestationResponse>();

		AttestationResponse attestationResponse ;
		for(Attestation a: attestations) {
			attestationResponse = attestationService.createAttestation(a);
			if(attestationResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(attestationResponse, HttpStatus.NOT_FOUND);
			}else {
				attestationResponses.add(attestationResponse);
			}
		}

		logger.log(Level.INFO, "------responses ok------");
		return  new ResponseEntity<>(attestationResponses, HttpStatus.OK);

		//return null;
	}

	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createAttestation(@RequestBody Attestation attestation){

		List<Attestation> attestations = attestationService.listAttestation(attestation.getNumero_attestation());
		AttestationResponse attestationResponse = null;
		if(attestations!=null && !attestations.isEmpty()) { //si l'attestation existe
			attestationResponse = attestationService.createAttestation(attestation);
			if(attestationResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(attestationResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(attestationResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(attestationResponse, HttpStatus.OK);
	}
	
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateAttestation(@RequestBody Attestation attestation){

		List<Attestation> attestations = attestationService.listAttestation(attestation.getNumero_attestation());
		AttestationResponse attestationResponse = null;
		if(attestations!=null && !attestations.isEmpty()) { //si l'attestation existe
			attestationResponse = attestationService.updateAttestation(attestations.get(0));
			if(attestationResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(attestationResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(attestationResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(attestationResponse, HttpStatus.OK);
	}

	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listAttestationsFromBD(){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Attestation> attestations = attestationService.listAttestation();

		if(attestations!=null && !attestations.isEmpty()) {

			respE = new ResponseEntity<>(attestations, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());

		}

		return respE;
	}

	@GetMapping(path="/list")
	public ResponseEntity<Object> listOneAttestationFromBD(@RequestParam("numero_attestation") String numero_attestation){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Attestation> attestations = attestationService.listAttestation(numero_attestation);
		if(attestations!=null && !attestations.isEmpty()) {
			respE = new ResponseEntity<>(attestations, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}

		return respE;
	}

}
