package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.services.interfaces.AssureService;

/**
 * API pour exposer les services de gestion des assurés
 * @author Stephane Mouafo
 * 
 *
 */
@RestController
@RequestMapping("/assure")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AssureController {

	@Autowired
	private AssureService assureService;

	private Logger logger = LogManager.getLogger(AssureController.class);

	/**
	 * Service de création de tous les assuré dans la base MINFI
	 * @return les assurés crées dans la base MINFI
	 */
	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createAssures() {

		logger.log(Level.INFO, "-----------------In Assure controller : createAssures()------------------");

		List<Assure> assures = assureService.listAssure();
		List<AssureResponse> assureResponses = new ArrayList<AssureResponse>();

		AssureResponse assureResponse ;

		for(Assure a: assures) {
			assureResponse = assureService.createAssure(a);
			if(assureResponse==null) {
				logger.log(Level.ERROR, "----------There was an error, check the assure and journal tables---");
			}else {
				assureResponses.add(assureResponse);
			}
		}

		logger.log(Level.INFO, "------------------Assure controller: createAssures() response returned--------------------" + HttpStatus.OK.toString());

		return  new ResponseEntity<>(assureResponses, HttpStatus.OK);

	}

	
	/**
	 * Service de création d'un assuré dans la base MINFI
	 * @param assure l'assuré que l'on souhaite crée
	 * @return l'assuré crée dans la base MINFI
	 */
	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createAssure(@RequestBody Assure assure){

		AssureResponse assureResponse = null;

		assureResponse = assureService.createAssure(assure);

		if(assureResponse==null) {
			logger.log(Level.ERROR, "-----------------The Assure " + assure.getNom() + " " + assure.getPrenom() + " was unable to be created. The create service returned null--------------");
			return new ResponseEntity<>(assureResponse, HttpStatus.NOT_FOUND);
		}else {
		}

		return new ResponseEntity<>(assureResponse, HttpStatus.OK);
	}

	/**
	 * Service de mise à jour d'un assuré dans la base MINFI
	 * @param assure l'assuré que l'on souhaite mettre à jour
	 * @return l'assuré mise à jour dans la base MINFI
	 */
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateAssure(@RequestBody Assure assure){

		List<Assure> assures = assureService.listAssure(assure.getCode_assure(), assure.getCode_assureur());
		AssureResponse assureResponse = null;
		if(assures!=null && !assures.isEmpty()) { //si l'assure existe
			assureResponse = assureService.updateAssure(assures.get(0));
			if(assureResponse==null) {
				logger.log(Level.ERROR, "-----------The Assure "   +  assure.getNom() + " " + assure.getPrenom() + " was unable to be updated. The update service returned null----------");
				return new ResponseEntity<>(assureResponse, HttpStatus.NOT_FOUND);
			}else {
			}
		}else {
			logger.log(Level.ERROR, "-----------The Assure "   +  assure.getNom() + " " + assure.getPrenom() + " does not exist----------");
			return new ResponseEntity<>(assureResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(assureResponse, HttpStatus.OK);
	}

	/**
	 * Service d'affichage de tous les assurés de la base de données interne
	 * @return tous les assurés de la base de données interne
	 */
	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listAssuresFromBD(){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Assure> assures = assureService.listAssure();

		if(assures!=null && !assures.isEmpty()) {

			respE = new ResponseEntity<>(assures, HttpStatus.OK);

			logger.log(Level.INFO, "--------------Response Entity Returned with status------------------"  + respE.getStatusCodeValue());

		}

		return respE;
	}

	/**
	 * Service d'affichage d'un assuré donné de la base de données interne
	 * @param code_assure le code assuré de l'assuré que l'on recherche
	 * @param code_assureur le code assureur de l'assuré que l'on recherche
	 * @return l'assuré recherché en interne
	 */
	@GetMapping(path="/listOneFromBD")
	public ResponseEntity<Object> listOneAssureFromBD(@RequestParam("code_assure") String code_assure, @RequestParam("code_assureur") String code_assureur){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Assure> assures = assureService.listAssure(code_assure, code_assureur);
		if(assures!=null && !assures.isEmpty()) {
			respE = new ResponseEntity<>(assures, HttpStatus.OK);

			logger.log(Level.INFO, "--------------Response Entity Returned with status------------------"  + respE.getStatusCodeValue());

		}

		return respE;
	}

	
	/**
	 * Service d'affichage d'un assuré donné de la base de données MINFI
	 * @param code_assure le code assuré de l'assuré que l'on recherche
	 * @param code_assureur le code assureur de l'assuré que l'on recherche
	 * @return l'assuré recherché dans la base MINFI
	 */
	@GetMapping(path="/list")
	public ResponseEntity<Object> listAssureFromMINFI(@RequestParam("code_assure") String code_assure, @RequestParam("code_assureur") String code_assureur){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		AssureResponse assures = assureService.listAssureFromMINFI(code_assure, code_assureur);
		if(assures!=null) {
			respE = new ResponseEntity<>(assures, HttpStatus.OK);
			logger.log(Level.INFO, "--------------Response Entity Returned with status------------------"  + respE.getStatusCodeValue());
		}

		return respE;

	}
}
