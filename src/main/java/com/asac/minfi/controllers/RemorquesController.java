package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Remorques;
import com.asac.minfi.entities.RemorquesResponse;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.RemorquesService;

@RestController
@RequestMapping("/remorques")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RemorquesController {

	@Autowired
	private RemorquesService remorquesService;
	
	private Logger logger = LogManager.getLogger(RemorquesController.class);


	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createRemorquess() { ////@RequestBody Remorques remorques

		logger.log(Level.INFO, "-----In remorques controller---");

		//Remorques remorques = getRemorquesFromBD();
		//logger.log(Level.INFO, "----Remorques selected---" + remorques.getNumero_contribuable());

		List<Remorques> remorquess = remorquesService.listRemorques();
		List<RemorquesResponse> remorquesResponses = new ArrayList<RemorquesResponse>();

		RemorquesResponse remorquesResponse ;
		for(Remorques a: remorquess) {
			remorquesResponse = remorquesService.createRemorques(a);
			if(remorquesResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(remorquesResponse, HttpStatus.NOT_FOUND);
			}else {
				remorquesResponses.add(remorquesResponse);
			}
		}

		logger.log(Level.INFO, "------responses ok------");
		return  new ResponseEntity<>(remorquesResponses, HttpStatus.OK);

		//return null;
	}

	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createRemorques(@RequestBody Remorques remorques){

		List<Remorques> remorquess = remorquesService.listRemorques(remorques.getImmatriculation_remorque());
		RemorquesResponse remorquesResponse = null;
		if(remorquess!=null && !remorquess.isEmpty()) { //si l'remorques existe
			remorquesResponse = remorquesService.createRemorques(remorques);
			if(remorquesResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(remorquesResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(remorquesResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(remorquesResponse, HttpStatus.OK);
	}
	
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateRemorques(@RequestBody Remorques remorques){

		List<Remorques> remorquess = remorquesService.listRemorques(remorques.getImmatriculation_remorque());
		RemorquesResponse remorquesResponse = null;
		if(remorquess!=null && !remorquess.isEmpty()) { //si l'remorques existe
			remorquesResponse = remorquesService.updateRemorques(remorques);
			if(remorquesResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(remorquesResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(remorquesResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(remorquesResponse, HttpStatus.OK);
	}

	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listRemorquessFromBD(){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Remorques> remorquess = remorquesService.listRemorques();

		if(remorquess!=null && !remorquess.isEmpty()) {

			respE = new ResponseEntity<>(remorquess, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());

		}

		return respE;
	}

	@GetMapping(path="/listOneFromBD")
	public ResponseEntity<Object> listOneRemorquesFromBD(@RequestParam("immatriculation_remorque") String immatriculation_remorque){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Remorques> remorquess = remorquesService.listRemorques(immatriculation_remorque);
		if(remorquess!=null && !remorquess.isEmpty()) {
			respE = new ResponseEntity<>(remorquess, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}

		return respE;
	}
	
	@GetMapping(path="/list")
	public ResponseEntity<Object> listRemorquesFromMINFI(@RequestParam("immatriculation_remorque") String immatriculation_remorque){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		RemorquesResponse remorquess = remorquesService.listRemorquesFromMINFI(immatriculation_remorque);
		if(remorquess!=null) {
			respE = new ResponseEntity<>(remorquess, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}

		return respE;
	}

}
