package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Intermediaire;
import com.asac.minfi.entities.IntermediaireResponse;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.IntermediaireService;

@RestController
@RequestMapping("/intermediaire")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class IntermediaireController {

	@Autowired
	private IntermediaireService intermediaireService;
	
	private Logger logger = LogManager.getLogger(IntermediaireController.class);


	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createIntermediaires() { ////@RequestBody Intermediaire intermediaire

		logger.log(Level.INFO, "-----In intermediaire controller---");

		//Intermediaire intermediaire = getIntermediaireFromBD();
		//logger.log(Level.INFO, "----Intermediaire selected---" + intermediaire.getNumero_contribuable());

		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire();
		List<IntermediaireResponse> intermediaireResponses = new ArrayList<IntermediaireResponse>();

		IntermediaireResponse intermediaireResponse ;
		for(Intermediaire a: intermediaires) {
			intermediaireResponse = intermediaireService.createIntermediaire(a);
			if(intermediaireResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(intermediaireResponse, HttpStatus.NOT_FOUND);
			}else {
				intermediaireResponses.add(intermediaireResponse);
			}
		}

		logger.log(Level.INFO, "------responses ok------");
		return  new ResponseEntity<>(intermediaireResponses, HttpStatus.OK);

		//return null;
	}

	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createIntermediaire(@RequestBody Intermediaire intermediaire){

		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire(intermediaire.getCode_intermediaire_dna());
		IntermediaireResponse intermediaireResponse = null;
		if(intermediaires!=null && !intermediaires.isEmpty()) { //si l'intermediaire existe
			intermediaireResponse = intermediaireService.createIntermediaire(intermediaire);
			if(intermediaireResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(intermediaireResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(intermediaireResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(intermediaireResponse, HttpStatus.OK);
	}
	
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateIntermediaire(@RequestBody Intermediaire intermediaire){

		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire(intermediaire.getCode_intermediaire_dna());
		IntermediaireResponse intermediaireResponse = null;
		if(intermediaires!=null && !intermediaires.isEmpty()) { //si l'intermediaire existe
			intermediaireResponse = intermediaireService.updateIntermediaire(intermediaire);
			if(intermediaireResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(intermediaireResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(intermediaireResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(intermediaireResponse, HttpStatus.OK);
	}

	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listIntermediairesFromBD(){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire();

		if(intermediaires!=null && !intermediaires.isEmpty()) {

			respE = new ResponseEntity<>(intermediaires, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());

		}

		return respE;
	}

	@GetMapping(path="/list")
	public ResponseEntity<Object> listOneIntermediaireFromBD(@RequestParam("identifiant_dna") String identifiant_dna){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Intermediaire> intermediaires = intermediaireService.listIntermediaire(identifiant_dna);
		if(intermediaires!=null && !intermediaires.isEmpty()) {
			respE = new ResponseEntity<>(intermediaires, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}

		return respE;
	}

}
