package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Vehicules;
import com.asac.minfi.entities.VehiculesResponse;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.VehiculesService;

/**
 * API pour exposer les services de gestion des véhicules
 * @author Stephane Mouafo
 * 
 *
 */
@RestController
@RequestMapping("/vehicule")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VehiculesController {

	@Autowired
	private VehiculesService vehiculeService;

	private Logger logger = LogManager.getLogger(VehiculesController.class);


	/**
	 * Service de création de tous les véhicules dans la base MINFI
	 * @return les véhicules crées dans la base MINFI
	 */
	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createVehiculess() {

		logger.log(Level.INFO, "---------------In vehicule controller: createVehicules()------------------");

		List<Vehicules> vehicules = vehiculeService.listVehicules();
		List<VehiculesResponse> vehiculeResponses = new ArrayList<VehiculesResponse>();

		VehiculesResponse vehiculeResponse ;
		for(Vehicules a: vehicules) {
			vehiculeResponse = vehiculeService.createVehicules(a);
			if(vehiculeResponse==null) {
				logger.log(Level.ERROR, "-------------There was an error, check the vehicule and journal tables-----------");
				return new ResponseEntity<>(vehiculeResponse, HttpStatus.NOT_FOUND);
			}else {
				vehiculeResponses.add(vehiculeResponse);
			}
		}

		logger.log(Level.INFO, "------------------Vehicule controller: createVehicules() response returned--------------------" + HttpStatus.OK.toString());
		return  new ResponseEntity<>(vehiculeResponses, HttpStatus.OK);

	}

	/**
	 * Service de création d'un véhicule dans la base MINFI
	 * @param vehicule le véhicule que l'on souhaite crée
	 * @return le véhicule crée dans la base MINFI
	 */
	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createVehicules(@RequestBody Vehicules vehicule){

		VehiculesResponse vehiculeResponse = null;
		
		vehiculeResponse = vehiculeService.createVehicules(vehicule);
		
		if(vehiculeResponse==null) {
			logger.log(Level.ERROR, "-----------------The Vehicule with immatriculation number " + vehicule.getImmatriculation() + " was unable to be created. The create service returned null--------------");
			return new ResponseEntity<>(vehiculeResponse, HttpStatus.NOT_FOUND);
		}else {
		}


		return new ResponseEntity<>(vehiculeResponse, HttpStatus.OK);
	}

	/**
	 * Service de mise à jour d'un véhicule dans la base MINFI
	 * @param vehicule le véhicule que l'on souhaite mettre à jour
	 * @return le véhicule mise à jour dans la base MINFI
	 */
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateVehicules(@RequestBody Vehicules vehicule){

		List<Vehicules> vehicules = vehiculeService.listVehicules(vehicule.getImmatriculation());
		VehiculesResponse vehiculeResponse = null;
		if(vehicules!=null && !vehicules.isEmpty()) { //si l'vehicule existe
			vehiculeResponse = vehiculeService.updateVehicules(vehicule);
			if(vehiculeResponse==null) {
				logger.log(Level.ERROR, "-----------------The Vehicule with immatriculation number " + vehicule.getImmatriculation() + " was unable to be created. The create service returned null--------------");
				return new ResponseEntity<>(vehiculeResponse, HttpStatus.NOT_FOUND);
			}else {
			}
		}else {
			return new ResponseEntity<>(vehiculeResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(vehiculeResponse, HttpStatus.OK);
	}

	/**
	 * Service d'affichage de tous les vehicules de la base de données interne
	 * @return tous les vehicules de la base de données internes
	 */
	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listVehiculessFromBD(){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Vehicules> vehicules = vehiculeService.listVehicules();

		if(vehicules!=null && !vehicules.isEmpty()) {

			respE = new ResponseEntity<>(vehicules, HttpStatus.OK);
			logger.log(Level.INFO, "--------------Response Entity Returned with status------------------"  + respE.getStatusCodeValue());
			
		}

		return respE;
	}

	/**
	 * Service d'affichage d'un véhicule de la base de données interne
	 * @param immatriculation l'immatriculation du véhicule que l'on recherche
	 * @return le véhicule recherché
	 */
	@GetMapping(path="/listOneFromBD")
	public ResponseEntity<Object> listOneVehiculesFromBD(@RequestParam("immatriculation") String immatriculation){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Vehicules> vehicules = vehiculeService.listVehicules(immatriculation);
		if(vehicules!=null && !vehicules.isEmpty()) {
			respE = new ResponseEntity<>(vehicules, HttpStatus.OK);
			logger.log(Level.INFO, "--------------Response Entity Returned with status------------------"  + respE.getStatusCodeValue());
		}

		return respE;
	}

	/**
	 * Service d'affichage d'un véhicule donné de la base de données MINFI
	 * @param immatriculation l'immatriculation du véhicule que l'on recherche
	 * @return le véhicule recherché dans la base MINFI
	 */
	@GetMapping(path="/list")
	public ResponseEntity<Object> listVehiculeFromMINFI(@RequestParam("immatriculation") String immatriculation){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		VehiculesResponse vehicules = vehiculeService.listVehiculeFromMINFI(immatriculation);
		if(vehicules!=null) {
			respE = new ResponseEntity<>(vehicules, HttpStatus.OK);
			logger.log(Level.INFO, "--------------Response Entity Returned with status------------------"  + respE.getStatusCodeValue());
		}

		return respE;
	}

}
