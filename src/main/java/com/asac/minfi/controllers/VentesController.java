package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Ventes;
import com.asac.minfi.entities.VentesResponse;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.VentesService;

@RestController
@RequestMapping("/ventes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VentesController {

	@Autowired
	private VentesService ventesService;
	
	private Logger logger = LogManager.getLogger(VentesController.class);


	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createVentess() { ////@RequestBody Ventes ventes

		logger.log(Level.INFO, "-----In ventes controller---");

		//Ventes ventes = getVentesFromBD();
		//logger.log(Level.INFO, "----Ventes selected---" + ventes.getNumero_contribuable());

		List<Ventes> ventess = ventesService.listVentes();
		List<VentesResponse> ventesResponses = new ArrayList<VentesResponse>();

		VentesResponse ventesResponse ;
		for(Ventes a: ventess) {
			ventesResponse = ventesService.createVentes(a);
			if(ventesResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(ventesResponse, HttpStatus.NOT_FOUND);
			}else {
				ventesResponses.add(ventesResponse);
			}
		}

		logger.log(Level.INFO, "------responses ok------");
		return  new ResponseEntity<>(ventesResponses, HttpStatus.OK);

		//return null;
	}

	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createVentes(@RequestBody Ventes ventes){

		List<Ventes> ventess = ventesService.listVentes(ventes.getImmatriculation());
		VentesResponse ventesResponse = null;
		if(ventess!=null && !ventess.isEmpty()) { //si l'ventes existe
			ventesResponse = ventesService.createVentes(ventes);
			if(ventesResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(ventesResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(ventesResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(ventesResponse, HttpStatus.OK);
	}
	
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateVentes(@RequestBody Ventes ventes){

		List<Ventes> ventess = ventesService.listVentes(ventes.getImmatriculation());
		VentesResponse ventesResponse = null;
		if(ventess!=null && !ventess.isEmpty()) { //si l'ventes existe
			ventesResponse = ventesService.updateVentes(ventes);
			if(ventesResponse==null) {
				logger.log(Level.ERROR, "-----response bad------");
				return new ResponseEntity<>(ventesResponse, HttpStatus.NOT_FOUND);
			}else {
				logger.log(Level.INFO, "------response ok------");
			}
		}else {
			return new ResponseEntity<>(ventesResponse, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(ventesResponse, HttpStatus.OK);
	}

	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listVentessFromBD(){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Ventes> ventess = ventesService.listVentes();

		if(ventess!=null && !ventess.isEmpty()) {

			respE = new ResponseEntity<>(ventess, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());

		}

		return respE;
	}

	@GetMapping(path="/list")
	public ResponseEntity<Object> listOneVentesFromBD(@RequestParam("immatriculation") String immatriculation){

		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);

		List<Ventes> ventess = ventesService.listVentes(immatriculation);
		if(ventess!=null && !ventess.isEmpty()) {
			respE = new ResponseEntity<>(ventess, HttpStatus.OK);
			logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}

		return respE;
	}

}
