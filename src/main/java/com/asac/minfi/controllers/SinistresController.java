package com.asac.minfi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asac.minfi.entities.Adresses;
import com.asac.minfi.entities.Assure;
import com.asac.minfi.entities.AssureResponse;
import com.asac.minfi.entities.Sinistres;
import com.asac.minfi.entities.SinistresResponse;
import com.asac.minfi.entities.Vehicules;
import com.asac.minfi.repositories.AssureRepository;
import com.asac.minfi.services.interfaces.AdressesService;
import com.asac.minfi.services.interfaces.SinistresService;

@RestController
@RequestMapping("/sinistres")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SinistresController {
	
	@Autowired
	private SinistresService sinistresService;
	
	private Logger logger = LogManager.getLogger(SinistresController.class);

	
	@PostMapping(path = "/createAll")
	public ResponseEntity<Object> createSinistres() { ////@RequestBody Sinistres sinistres
		

		logger.log(Level.INFO, "-----In sinistres controller---");
		
		 //Sinistres sinistres = getSinistresFromBD();
		 //logger.log(Level.INFO, "----Sinistres selected---" + sinistres.getReference());
		
		List<Sinistres> sinistres = sinistresService.listSinistres();
		List<SinistresResponse> sinistreResponses = new ArrayList<SinistresResponse>();
		
		
		 SinistresResponse sinistresResponse;
		 for(Sinistres s: sinistres) {
			 sinistresResponse = sinistresService.createSinistres(s);
			 if(sinistresResponse==null) {
				 logger.log(Level.ERROR, "-----response bad------");
				 return new ResponseEntity<>(sinistresResponse, HttpStatus.NOT_FOUND);
			 }else {
				 sinistreResponses.add(sinistresResponse);
			 }
		 }
		
		 
		 logger.log(Level.INFO, "------response ok------");
		 return  new ResponseEntity<>(sinistreResponses, HttpStatus.OK);
		 
		 //return null;
	}
	
	@PostMapping(path = "/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> createSinistre(@RequestBody Sinistres sinistres) { ////@RequestBody Sinistres sinistres
		
		List<Sinistres> sins = sinistresService.listSinistres(sinistres.getNumero_attestation(), sinistres.getReference());
		SinistresResponse sinistresResponse = null;
		if(sins!=null && !sins.isEmpty()) { //si le sinistre existe
		  sinistresResponse = sinistresService.createSinistres(sinistres);
			 if(sinistresResponse==null) {
				 logger.log(Level.ERROR, "-----response bad------");
				 return new ResponseEntity<>(sinistresResponse, HttpStatus.NOT_FOUND);
			 }else {
				 logger.log(Level.INFO, "------response ok------");
			 }
		}else {
			 return new ResponseEntity<>(sinistresResponse, HttpStatus.NOT_FOUND);
		}
		 
		 return  new ResponseEntity<>(sinistresResponse, HttpStatus.OK);
		 
	}
	
	@PutMapping(path = "/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateSinistre(@RequestBody Sinistres sinistres){
		
		List<Sinistres> sins = sinistresService.listSinistres(sinistres.getNumero_attestation(), sinistres.getReference());
		SinistresResponse sinistresResponse = null;
		if(sins!=null && !sins.isEmpty()) { //si le sinistre existe
		  sinistresResponse = sinistresService.updateSinistres(sinistres);
			 if(sinistresResponse==null) {
				 logger.log(Level.ERROR, "-----response bad------");
				 return new ResponseEntity<>(sinistresResponse, HttpStatus.NOT_FOUND);
			 }else {
				 logger.log(Level.INFO, "------response ok------");
			 }
		}else {
			 return new ResponseEntity<>(sinistresResponse, HttpStatus.NOT_FOUND);
		}
		 
		 return  new ResponseEntity<>(sinistresResponse, HttpStatus.OK);
	}
	
	@GetMapping(path="/listAllFromBD")
	public ResponseEntity<Object> listSinistresFromBD(){
		
		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		List<Sinistres> sinistres = sinistresService.listSinistres();
		
		if(sinistres!=null && !sinistres.isEmpty()) {
			
		   respE = new ResponseEntity<>(sinistres, HttpStatus.OK);
		   logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
		   logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
			
		}
		
		return respE;
	}
	
	@GetMapping(path="/listOneFromBD")
	public ResponseEntity<Object> listOneSinistreFromBD(@RequestParam("numero_attestation") String numero_attestation, @RequestParam("reference") String reference){
		
		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		List<Sinistres> sinistres = sinistresService.listSinistres(numero_attestation, reference);
		if(sinistres!=null && !sinistres.isEmpty()) {
			  respE = new ResponseEntity<>(sinistres, HttpStatus.OK);
			   logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			   logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}
		
		return respE;
	}

	@GetMapping(path="/list")
	public ResponseEntity<Object> listSinistreFromMINFI(@RequestParam("numero_attestation") String numero_attestation, @RequestParam("reference") String reference){
		
		ResponseEntity<Object> respE = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		SinistresResponse sinistres = sinistresService.listSinistreFromMINFI(numero_attestation, reference);
		if(sinistres!=null) {
			  respE = new ResponseEntity<>(sinistres, HttpStatus.OK);
			   logger.log(Level.INFO, "----respE.getBody().toString()---" + respE.getBody().toString());
			   logger.log(Level.INFO, "-----respE.toString()----" + respE.toString());
		}
		
		return respE;
	}


}
