FROM azul/zulu-openjdk-alpine:11.0.6
COPY target/application.properties application.properties
COPY target/hub-asac-minfi-api-client-0.0.1-SNAPSHOT.jar minfi-api-client.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar",\
"-Dspring.config.location=./application.properties",\
 "/minfi-api-client.jar"]
